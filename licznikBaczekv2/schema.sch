<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Q4" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_5" />
        <signal name="Q2" />
        <signal name="XLXN_8" />
        <signal name="Q1" />
        <signal name="XLXN_10" />
        <signal name="XLXN_12" />
        <signal name="XLXN_14" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="ZEGAR" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="Q3" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <port polarity="Output" name="Q4" />
        <port polarity="Output" name="Q2" />
        <port polarity="Output" name="Q1" />
        <port polarity="Input" name="ZEGAR" />
        <port polarity="Output" name="Q3" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <block symbolname="fd" name="XLXI_1">
            <attr value="1" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="ZEGAR" name="C" />
            <blockpin signalname="XLXN_22" name="D" />
            <blockpin signalname="Q1" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_2">
            <attr value="1" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="ZEGAR" name="C" />
            <blockpin signalname="XLXN_23" name="D" />
            <blockpin signalname="Q2" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <attr value="0" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="ZEGAR" name="C" />
            <blockpin signalname="XLXN_24" name="D" />
            <blockpin signalname="Q3" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_4">
            <attr value="0" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="ZEGAR" name="C" />
            <blockpin signalname="XLXN_26" name="D" />
            <blockpin signalname="Q4" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_13">
            <blockpin signalname="Q1" name="I" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_17">
            <blockpin signalname="Q2" name="I" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_18">
            <blockpin signalname="Q3" name="I" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_19">
            <blockpin signalname="Q4" name="I" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_20">
            <blockpin signalname="XLXN_27" name="I0" />
            <blockpin signalname="XLXN_28" name="I1" />
            <blockpin signalname="XLXN_22" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_21">
            <blockpin signalname="XLXN_29" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="XLXN_31" name="I2" />
            <blockpin signalname="XLXN_32" name="I3" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_22">
            <blockpin signalname="XLXN_34" name="I0" />
            <blockpin signalname="XLXN_35" name="I1" />
            <blockpin signalname="XLXN_36" name="I2" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_23">
            <blockpin signalname="XLXN_37" name="I0" />
            <blockpin signalname="XLXN_38" name="I1" />
            <blockpin signalname="XLXN_26" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_24">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="Q3" name="I1" />
            <blockpin signalname="XLXN_8" name="I2" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_25">
            <blockpin signalname="Q4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_8" name="I2" />
            <blockpin signalname="XLXN_28" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_26">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="Q3" name="I1" />
            <blockpin signalname="XLXN_5" name="I2" />
            <blockpin signalname="Q1" name="I3" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_27">
            <blockpin signalname="Q4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="Q2" name="I2" />
            <blockpin signalname="XLXN_8" name="I3" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_28">
            <blockpin signalname="Q4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_5" name="I2" />
            <blockpin signalname="Q1" name="I3" />
            <blockpin signalname="XLXN_31" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_29">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="Q3" name="I1" />
            <blockpin signalname="Q2" name="I2" />
            <blockpin signalname="XLXN_8" name="I3" />
            <blockpin signalname="XLXN_32" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_30">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="Q3" name="I1" />
            <blockpin signalname="Q2" name="I2" />
            <blockpin signalname="XLXN_8" name="I3" />
            <blockpin signalname="XLXN_36" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_31">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="Q2" name="I2" />
            <blockpin signalname="Q1" name="I3" />
            <blockpin signalname="XLXN_35" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_32">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="Q3" name="I1" />
            <blockpin signalname="XLXN_5" name="I2" />
            <blockpin signalname="XLXN_34" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_33">
            <blockpin signalname="Q4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_5" name="I2" />
            <blockpin signalname="XLXN_37" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_35">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="Q3" name="I1" />
            <blockpin signalname="Q2" name="I2" />
            <blockpin signalname="Q1" name="I3" />
            <blockpin signalname="XLXN_38" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="XLXN_2">
            <wire x2="544" y1="592" y2="592" x1="432" />
            <wire x2="544" y1="592" y2="784" x1="544" />
            <wire x2="976" y1="592" y2="592" x1="544" />
            <wire x2="976" y1="592" y2="704" x1="976" />
            <wire x2="1632" y1="592" y2="592" x1="976" />
            <wire x2="1984" y1="592" y2="592" x1="1632" />
            <wire x2="1984" y1="592" y2="672" x1="1984" />
            <wire x2="2160" y1="592" y2="592" x1="1984" />
            <wire x2="2160" y1="592" y2="672" x1="2160" />
            <wire x2="2400" y1="592" y2="592" x1="2160" />
            <wire x2="2400" y1="592" y2="672" x1="2400" />
            <wire x2="2960" y1="592" y2="592" x1="2400" />
            <wire x2="3376" y1="592" y2="592" x1="2960" />
            <wire x2="3440" y1="592" y2="592" x1="3376" />
            <wire x2="2960" y1="592" y2="784" x1="2960" />
            <wire x2="1632" y1="592" y2="688" x1="1632" />
            <wire x2="3376" y1="384" y2="592" x1="3376" />
        </branch>
        <instance x="544" y="1792" name="XLXI_1" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-408" type="instance" />
        </instance>
        <instance x="1440" y="1792" name="XLXI_2" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-408" type="instance" />
        </instance>
        <instance x="2144" y="1792" name="XLXI_3" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-408" type="instance" />
        </instance>
        <instance x="2960" y="1808" name="XLXI_4" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-408" type="instance" />
        </instance>
        <instance x="848" y="160" name="XLXI_13" orien="R90" />
        <instance x="3344" y="160" name="XLXI_19" orien="R90" />
        <instance x="2528" y="160" name="XLXI_18" orien="R90" />
        <instance x="1760" y="160" name="XLXI_17" orien="R90" />
        <branch name="ZEGAR">
            <wire x2="544" y1="1840" y2="1840" x1="448" />
            <wire x2="1440" y1="1840" y2="1840" x1="544" />
            <wire x2="2160" y1="1840" y2="1840" x1="1440" />
            <wire x2="2960" y1="1840" y2="1840" x1="2160" />
            <wire x2="3392" y1="1840" y2="1840" x1="2960" />
            <wire x2="3392" y1="1840" y2="1856" x1="3392" />
            <wire x2="544" y1="1664" y2="1664" x1="464" />
            <wire x2="464" y1="1664" y2="1792" x1="464" />
            <wire x2="544" y1="1792" y2="1792" x1="464" />
            <wire x2="544" y1="1792" y2="1840" x1="544" />
            <wire x2="1440" y1="1664" y2="1664" x1="1376" />
            <wire x2="1376" y1="1664" y2="1792" x1="1376" />
            <wire x2="1440" y1="1792" y2="1792" x1="1376" />
            <wire x2="1440" y1="1792" y2="1840" x1="1440" />
            <wire x2="2144" y1="1664" y2="1664" x1="2080" />
            <wire x2="2080" y1="1664" y2="1792" x1="2080" />
            <wire x2="2160" y1="1792" y2="1792" x1="2080" />
            <wire x2="2160" y1="1792" y2="1840" x1="2160" />
            <wire x2="2960" y1="1680" y2="1680" x1="2896" />
            <wire x2="2896" y1="1680" y2="1808" x1="2896" />
            <wire x2="2960" y1="1808" y2="1808" x1="2896" />
            <wire x2="2960" y1="1808" y2="1840" x1="2960" />
        </branch>
        <iomarker fontsize="28" x="448" y="1840" name="ZEGAR" orien="R180" />
        <branch name="XLXN_22">
            <wire x2="528" y1="1456" y2="1536" x1="528" />
            <wire x2="544" y1="1536" y2="1536" x1="528" />
            <wire x2="560" y1="1456" y2="1456" x1="528" />
            <wire x2="560" y1="1424" y2="1456" x1="560" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="1424" y1="1440" y2="1536" x1="1424" />
            <wire x2="1440" y1="1536" y2="1536" x1="1424" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="2128" y1="1408" y2="1536" x1="2128" />
            <wire x2="2144" y1="1536" y2="1536" x1="2128" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="2944" y1="1472" y2="1552" x1="2944" />
            <wire x2="2960" y1="1552" y2="1552" x1="2944" />
            <wire x2="2960" y1="1472" y2="1472" x1="2944" />
            <wire x2="2960" y1="1456" y2="1472" x1="2960" />
        </branch>
        <instance x="464" y="1168" name="XLXI_20" orien="R90" />
        <instance x="1264" y="1184" name="XLXI_21" orien="R90" />
        <instance x="2000" y="1152" name="XLXI_22" orien="R90" />
        <instance x="2864" y="1200" name="XLXI_23" orien="R90" />
        <instance x="704" y="784" name="XLXI_25" orien="R90" />
        <instance x="480" y="784" name="XLXI_24" orien="R90" />
        <branch name="XLXN_27">
            <wire x2="528" y1="1104" y2="1168" x1="528" />
            <wire x2="608" y1="1104" y2="1104" x1="528" />
            <wire x2="608" y1="1040" y2="1104" x1="608" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="592" y1="1120" y2="1168" x1="592" />
            <wire x2="832" y1="1120" y2="1120" x1="592" />
            <wire x2="832" y1="1040" y2="1120" x1="832" />
        </branch>
        <instance x="912" y="704" name="XLXI_26" orien="R90" />
        <instance x="1136" y="704" name="XLXI_27" orien="R90" />
        <instance x="1344" y="704" name="XLXI_28" orien="R90" />
        <branch name="XLXN_29">
            <wire x2="1072" y1="960" y2="1184" x1="1072" />
            <wire x2="1328" y1="1184" y2="1184" x1="1072" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="1296" y1="960" y2="1072" x1="1296" />
            <wire x2="1392" y1="1072" y2="1072" x1="1296" />
            <wire x2="1392" y1="1072" y2="1184" x1="1392" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="1456" y1="1072" y2="1184" x1="1456" />
            <wire x2="1504" y1="1072" y2="1072" x1="1456" />
            <wire x2="1504" y1="960" y2="1072" x1="1504" />
        </branch>
        <branch name="XLXN_32">
            <wire x2="1536" y1="1184" y2="1184" x1="1520" />
            <wire x2="1728" y1="1184" y2="1184" x1="1536" />
            <wire x2="1728" y1="944" y2="1184" x1="1728" />
        </branch>
        <branch name="Q3">
            <wire x2="608" y1="528" y2="528" x1="432" />
            <wire x2="608" y1="528" y2="784" x1="608" />
            <wire x2="1040" y1="528" y2="528" x1="608" />
            <wire x2="1040" y1="528" y2="704" x1="1040" />
            <wire x2="1680" y1="528" y2="528" x1="1040" />
            <wire x2="1688" y1="528" y2="528" x1="1680" />
            <wire x2="2048" y1="528" y2="528" x1="1688" />
            <wire x2="2048" y1="528" y2="672" x1="2048" />
            <wire x2="2464" y1="528" y2="528" x1="2048" />
            <wire x2="2624" y1="528" y2="528" x1="2464" />
            <wire x2="2624" y1="528" y2="1536" x1="2624" />
            <wire x2="3024" y1="528" y2="528" x1="2624" />
            <wire x2="3424" y1="528" y2="528" x1="3024" />
            <wire x2="3024" y1="528" y2="784" x1="3024" />
            <wire x2="2464" y1="528" y2="672" x1="2464" />
            <wire x2="1680" y1="528" y2="608" x1="1680" />
            <wire x2="1696" y1="608" y2="608" x1="1680" />
            <wire x2="1696" y1="608" y2="688" x1="1696" />
            <wire x2="2624" y1="1536" y2="1536" x1="2528" />
            <wire x2="2560" y1="128" y2="160" x1="2560" />
            <wire x2="2624" y1="128" y2="128" x1="2560" />
            <wire x2="2624" y1="128" y2="528" x1="2624" />
            <wire x2="2656" y1="128" y2="128" x1="2624" />
            <wire x2="3424" y1="512" y2="528" x1="3424" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1104" y1="480" y2="480" x1="432" />
            <wire x2="1104" y1="480" y2="704" x1="1104" />
            <wire x2="1536" y1="480" y2="480" x1="1104" />
            <wire x2="1536" y1="480" y2="704" x1="1536" />
            <wire x2="1760" y1="480" y2="480" x1="1536" />
            <wire x2="1792" y1="480" y2="480" x1="1760" />
            <wire x2="2112" y1="480" y2="480" x1="1792" />
            <wire x2="2112" y1="480" y2="672" x1="2112" />
            <wire x2="2912" y1="480" y2="480" x1="2112" />
            <wire x2="3424" y1="480" y2="480" x1="2912" />
            <wire x2="2912" y1="480" y2="784" x1="2912" />
            <wire x2="1792" y1="384" y2="480" x1="1792" />
        </branch>
        <branch name="Q1">
            <wire x2="928" y1="416" y2="416" x1="432" />
            <wire x2="1152" y1="416" y2="416" x1="928" />
            <wire x2="1160" y1="416" y2="416" x1="1152" />
            <wire x2="1600" y1="416" y2="416" x1="1160" />
            <wire x2="1600" y1="416" y2="704" x1="1600" />
            <wire x2="1824" y1="416" y2="416" x1="1600" />
            <wire x2="2352" y1="416" y2="416" x1="1824" />
            <wire x2="2352" y1="416" y2="672" x1="2352" />
            <wire x2="3152" y1="416" y2="416" x1="2352" />
            <wire x2="3424" y1="416" y2="416" x1="3152" />
            <wire x2="3152" y1="416" y2="784" x1="3152" />
            <wire x2="1152" y1="416" y2="512" x1="1152" />
            <wire x2="1168" y1="512" y2="512" x1="1152" />
            <wire x2="1168" y1="512" y2="704" x1="1168" />
            <wire x2="880" y1="144" y2="160" x1="880" />
            <wire x2="944" y1="144" y2="144" x1="880" />
            <wire x2="944" y1="144" y2="272" x1="944" />
            <wire x2="944" y1="272" y2="1536" x1="944" />
            <wire x2="992" y1="144" y2="144" x1="944" />
            <wire x2="944" y1="272" y2="272" x1="928" />
            <wire x2="928" y1="272" y2="416" x1="928" />
            <wire x2="944" y1="1536" y2="1536" x1="928" />
            <wire x2="3424" y1="400" y2="416" x1="3424" />
        </branch>
        <branch name="Q4">
            <wire x2="768" y1="576" y2="576" x1="432" />
            <wire x2="768" y1="576" y2="784" x1="768" />
            <wire x2="1200" y1="576" y2="576" x1="768" />
            <wire x2="1200" y1="576" y2="704" x1="1200" />
            <wire x2="1408" y1="576" y2="576" x1="1200" />
            <wire x2="1408" y1="576" y2="704" x1="1408" />
            <wire x2="1632" y1="576" y2="576" x1="1408" />
            <wire x2="2784" y1="576" y2="576" x1="1632" />
            <wire x2="3328" y1="576" y2="576" x1="2784" />
            <wire x2="3440" y1="576" y2="576" x1="3328" />
            <wire x2="3328" y1="576" y2="672" x1="3328" />
            <wire x2="3408" y1="672" y2="672" x1="3328" />
            <wire x2="3408" y1="672" y2="1552" x1="3408" />
            <wire x2="2784" y1="576" y2="784" x1="2784" />
            <wire x2="3376" y1="128" y2="128" x1="3328" />
            <wire x2="3376" y1="128" y2="160" x1="3376" />
            <wire x2="3408" y1="128" y2="128" x1="3376" />
            <wire x2="3328" y1="128" y2="576" x1="3328" />
            <wire x2="3408" y1="1552" y2="1552" x1="3344" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="832" y1="544" y2="544" x1="432" />
            <wire x2="832" y1="544" y2="784" x1="832" />
            <wire x2="1264" y1="544" y2="544" x1="832" />
            <wire x2="1264" y1="544" y2="704" x1="1264" />
            <wire x2="1472" y1="544" y2="544" x1="1264" />
            <wire x2="1472" y1="544" y2="704" x1="1472" />
            <wire x2="1696" y1="544" y2="544" x1="1472" />
            <wire x2="2224" y1="544" y2="544" x1="1696" />
            <wire x2="2560" y1="544" y2="544" x1="2224" />
            <wire x2="2848" y1="544" y2="544" x1="2560" />
            <wire x2="3424" y1="544" y2="544" x1="2848" />
            <wire x2="2848" y1="544" y2="784" x1="2848" />
            <wire x2="2224" y1="544" y2="672" x1="2224" />
            <wire x2="2560" y1="384" y2="544" x1="2560" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="672" y1="432" y2="432" x1="432" />
            <wire x2="672" y1="432" y2="784" x1="672" />
            <wire x2="880" y1="432" y2="432" x1="672" />
            <wire x2="896" y1="432" y2="432" x1="880" />
            <wire x2="896" y1="432" y2="784" x1="896" />
            <wire x2="1392" y1="432" y2="432" x1="896" />
            <wire x2="1392" y1="432" y2="704" x1="1392" />
            <wire x2="1824" y1="432" y2="432" x1="1392" />
            <wire x2="2592" y1="432" y2="432" x1="1824" />
            <wire x2="3424" y1="432" y2="432" x1="2592" />
            <wire x2="2592" y1="432" y2="672" x1="2592" />
            <wire x2="1824" y1="432" y2="688" x1="1824" />
            <wire x2="880" y1="384" y2="432" x1="880" />
        </branch>
        <branch name="Q2">
            <wire x2="1168" y1="464" y2="464" x1="432" />
            <wire x2="1328" y1="464" y2="464" x1="1168" />
            <wire x2="1328" y1="464" y2="704" x1="1328" />
            <wire x2="1768" y1="464" y2="464" x1="1328" />
            <wire x2="1776" y1="464" y2="464" x1="1768" />
            <wire x2="1872" y1="464" y2="464" x1="1776" />
            <wire x2="2288" y1="464" y2="464" x1="1872" />
            <wire x2="2288" y1="464" y2="672" x1="2288" />
            <wire x2="2528" y1="464" y2="464" x1="2288" />
            <wire x2="2528" y1="464" y2="672" x1="2528" />
            <wire x2="3088" y1="464" y2="464" x1="2528" />
            <wire x2="3424" y1="464" y2="464" x1="3088" />
            <wire x2="3088" y1="464" y2="784" x1="3088" />
            <wire x2="1776" y1="464" y2="512" x1="1776" />
            <wire x2="1776" y1="512" y2="512" x1="1760" />
            <wire x2="1760" y1="512" y2="688" x1="1760" />
            <wire x2="1792" y1="144" y2="160" x1="1792" />
            <wire x2="1888" y1="144" y2="144" x1="1792" />
            <wire x2="1888" y1="144" y2="320" x1="1888" />
            <wire x2="1888" y1="320" y2="1536" x1="1888" />
            <wire x2="1904" y1="144" y2="144" x1="1888" />
            <wire x2="1888" y1="1536" y2="1536" x1="1824" />
            <wire x2="1888" y1="320" y2="320" x1="1872" />
            <wire x2="1872" y1="320" y2="464" x1="1872" />
        </branch>
        <instance x="2336" y="672" name="XLXI_30" orien="R90" />
        <instance x="2096" y="672" name="XLXI_31" orien="R90" />
        <instance x="1920" y="672" name="XLXI_32" orien="R90" />
        <branch name="XLXN_34">
            <wire x2="2048" y1="928" y2="1152" x1="2048" />
            <wire x2="2064" y1="1152" y2="1152" x1="2048" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="2128" y1="1040" y2="1152" x1="2128" />
            <wire x2="2256" y1="1040" y2="1040" x1="2128" />
            <wire x2="2256" y1="928" y2="1040" x1="2256" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="2496" y1="1152" y2="1152" x1="2192" />
            <wire x2="2496" y1="928" y2="1152" x1="2496" />
        </branch>
        <instance x="2720" y="784" name="XLXI_33" orien="R90" />
        <instance x="2896" y="784" name="XLXI_35" orien="R90" />
        <branch name="XLXN_37">
            <wire x2="2848" y1="1040" y2="1120" x1="2848" />
            <wire x2="2928" y1="1120" y2="1120" x1="2848" />
            <wire x2="2928" y1="1120" y2="1200" x1="2928" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="2992" y1="1120" y2="1200" x1="2992" />
            <wire x2="3056" y1="1120" y2="1120" x1="2992" />
            <wire x2="3056" y1="1040" y2="1120" x1="3056" />
        </branch>
        <iomarker fontsize="28" x="992" y="144" name="Q1" orien="R0" />
        <iomarker fontsize="28" x="1904" y="144" name="Q2" orien="R0" />
        <iomarker fontsize="28" x="2656" y="128" name="Q3" orien="R0" />
        <iomarker fontsize="28" x="3408" y="128" name="Q4" orien="R0" />
        <instance x="1568" y="688" name="XLXI_29" orien="R90" />
    </sheet>
</drawing>