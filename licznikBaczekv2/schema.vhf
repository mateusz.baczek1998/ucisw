--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : schema.vhf
-- /___/   /\     Timestamp : 11/12/2019 20:15:18
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl C:/Users/lab/Downloads/ucisw-master/ucisw-master/licznikBaczekv2/schema.vhf -w C:/Users/lab/Downloads/ucisw-master/ucisw-master/licznikBaczekv2/schema.sch
--Design Name: schema
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FD_MXILINX_schema is
   generic( INIT : bit :=  '0');
   port ( C : in    std_logic; 
          D : in    std_logic; 
          Q : out   std_logic);
end FD_MXILINX_schema;

architecture BEHAVIORAL of FD_MXILINX_schema is
   attribute BOX_TYPE   : string ;
   signal XLXN_4 : std_logic;
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
begin
   I_36_43 : GND
      port map (G=>XLXN_4);
   
   U0 : FDCP
   generic map( INIT => INIT)
      port map (C=>C,
                CLR=>XLXN_4,
                D=>D,
                PRE=>XLXN_4,
                Q=>Q);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity schema is
   port ( ZEGAR : in    std_logic; 
          Q1    : out   std_logic; 
          Q2    : out   std_logic; 
          Q3    : out   std_logic; 
          Q4    : out   std_logic);
end schema;

architecture BEHAVIORAL of schema is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_2   : std_logic;
   signal XLXN_3   : std_logic;
   signal XLXN_5   : std_logic;
   signal XLXN_8   : std_logic;
   signal XLXN_22  : std_logic;
   signal XLXN_23  : std_logic;
   signal XLXN_24  : std_logic;
   signal XLXN_26  : std_logic;
   signal XLXN_27  : std_logic;
   signal XLXN_28  : std_logic;
   signal XLXN_29  : std_logic;
   signal XLXN_30  : std_logic;
   signal XLXN_31  : std_logic;
   signal XLXN_32  : std_logic;
   signal XLXN_34  : std_logic;
   signal XLXN_35  : std_logic;
   signal XLXN_36  : std_logic;
   signal XLXN_37  : std_logic;
   signal XLXN_38  : std_logic;
   signal Q1_DUMMY : std_logic;
   signal Q2_DUMMY : std_logic;
   signal Q3_DUMMY : std_logic;
   signal Q4_DUMMY : std_logic;
   component FD_MXILINX_schema
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
   component OR3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR3 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND4 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_1 : label is "XLXI_1_0";
   attribute HU_SET of XLXI_2 : label is "XLXI_2_1";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_2";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_3";
begin
   Q1 <= Q1_DUMMY;
   Q2 <= Q2_DUMMY;
   Q3 <= Q3_DUMMY;
   Q4 <= Q4_DUMMY;
   XLXI_1 : FD_MXILINX_schema
   generic map( INIT => '1')
      port map (C=>ZEGAR,
                D=>XLXN_22,
                Q=>Q1_DUMMY);
   
   XLXI_2 : FD_MXILINX_schema
   generic map( INIT => '1')
      port map (C=>ZEGAR,
                D=>XLXN_23,
                Q=>Q2_DUMMY);
   
   XLXI_3 : FD_MXILINX_schema
   generic map( INIT => '0')
      port map (C=>ZEGAR,
                D=>XLXN_24,
                Q=>Q3_DUMMY);
   
   XLXI_4 : FD_MXILINX_schema
   generic map( INIT => '0')
      port map (C=>ZEGAR,
                D=>XLXN_26,
                Q=>Q4_DUMMY);
   
   XLXI_13 : INV
      port map (I=>Q1_DUMMY,
                O=>XLXN_8);
   
   XLXI_17 : INV
      port map (I=>Q2_DUMMY,
                O=>XLXN_5);
   
   XLXI_18 : INV
      port map (I=>Q3_DUMMY,
                O=>XLXN_3);
   
   XLXI_19 : INV
      port map (I=>Q4_DUMMY,
                O=>XLXN_2);
   
   XLXI_20 : OR2
      port map (I0=>XLXN_27,
                I1=>XLXN_28,
                O=>XLXN_22);
   
   XLXI_21 : OR4
      port map (I0=>XLXN_29,
                I1=>XLXN_30,
                I2=>XLXN_31,
                I3=>XLXN_32,
                O=>XLXN_23);
   
   XLXI_22 : OR3
      port map (I0=>XLXN_34,
                I1=>XLXN_35,
                I2=>XLXN_36,
                O=>XLXN_24);
   
   XLXI_23 : OR2
      port map (I0=>XLXN_37,
                I1=>XLXN_38,
                O=>XLXN_26);
   
   XLXI_24 : AND3
      port map (I0=>XLXN_2,
                I1=>Q3_DUMMY,
                I2=>XLXN_8,
                O=>XLXN_27);
   
   XLXI_25 : AND3
      port map (I0=>Q4_DUMMY,
                I1=>XLXN_3,
                I2=>XLXN_8,
                O=>XLXN_28);
   
   XLXI_26 : AND4
      port map (I0=>XLXN_2,
                I1=>Q3_DUMMY,
                I2=>XLXN_5,
                I3=>Q1_DUMMY,
                O=>XLXN_29);
   
   XLXI_27 : AND4
      port map (I0=>Q4_DUMMY,
                I1=>XLXN_3,
                I2=>Q2_DUMMY,
                I3=>XLXN_8,
                O=>XLXN_30);
   
   XLXI_28 : AND4
      port map (I0=>Q4_DUMMY,
                I1=>XLXN_3,
                I2=>XLXN_5,
                I3=>Q1_DUMMY,
                O=>XLXN_31);
   
   XLXI_29 : AND4
      port map (I0=>XLXN_2,
                I1=>Q3_DUMMY,
                I2=>Q2_DUMMY,
                I3=>XLXN_8,
                O=>XLXN_32);
   
   XLXI_30 : AND4
      port map (I0=>XLXN_2,
                I1=>Q3_DUMMY,
                I2=>Q2_DUMMY,
                I3=>XLXN_8,
                O=>XLXN_36);
   
   XLXI_31 : AND4
      port map (I0=>XLXN_2,
                I1=>XLXN_3,
                I2=>Q2_DUMMY,
                I3=>Q1_DUMMY,
                O=>XLXN_35);
   
   XLXI_32 : AND3
      port map (I0=>XLXN_2,
                I1=>Q3_DUMMY,
                I2=>XLXN_5,
                O=>XLXN_34);
   
   XLXI_33 : AND3
      port map (I0=>Q4_DUMMY,
                I1=>XLXN_3,
                I2=>XLXN_5,
                O=>XLXN_37);
   
   XLXI_35 : AND4
      port map (I0=>XLXN_2,
                I1=>Q3_DUMMY,
                I2=>Q2_DUMMY,
                I3=>Q1_DUMMY,
                O=>XLXN_38);
   
end BEHAVIORAL;


