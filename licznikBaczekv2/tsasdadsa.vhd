-- Vhdl test bench created from schematic C:\Users\lab\Downloads\ucisw-master\ucisw-master\licznikBaczekv2\schema.sch - Tue Nov 12 20:09:04 2019
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY schema_schema_sch_tb IS
END schema_schema_sch_tb;
ARCHITECTURE behavioral OF schema_schema_sch_tb IS 

   COMPONENT schema
   PORT( Q4	:	OUT	STD_LOGIC; 
          Q2	:	OUT	STD_LOGIC; 
          Q1	:	OUT	STD_LOGIC; 
          ZEGAR	:	IN	STD_LOGIC; 
          Q3	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL Q4	:	STD_LOGIC;
   SIGNAL Q2	:	STD_LOGIC;
   SIGNAL Q1	:	STD_LOGIC;
   SIGNAL ZEGAR	:	STD_LOGIC;
   SIGNAL Q3	:	STD_LOGIC;

BEGIN

   UUT: schema PORT MAP(
		Q4 => Q4, 
		Q2 => Q2, 
		Q1 => Q1, 
		ZEGAR => ZEGAR, 
		Q3 => Q3
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
			ZEGAR <= '0', '1' after 100ns, '0' after 200 ns, '1' after 300 ns, '0' after 400 ns, '1' after 500 ns, '0' after 600 ns, '1' after 700 ns, '0' after 800 ns, '1' after 900 ns, '0' after 1000 ns, '1' after 1100 ns, '0' after 1200 ns, '1' after 1300 ns, '0' after 1400 ns, '1' after 1500 ns;


      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
