-- Vhdl test bench created from schematic C:\Users\lab\konwerter\schema.sch - Tue Oct 29 20:07:18 2019
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY schema_schema_sch_tb IS
END schema_schema_sch_tb;
ARCHITECTURE behavioral OF schema_schema_sch_tb IS 

   COMPONENT schema
   PORT( A	:	IN	STD_LOGIC; 
          B	:	IN	STD_LOGIC; 
          C	:	IN	STD_LOGIC; 
          D	:	IN	STD_LOGIC; 
          Z	:	OUT	STD_LOGIC; 
          Y	:	OUT	STD_LOGIC; 
          X	:	OUT	STD_LOGIC; 
          W	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL A	:	STD_LOGIC;
   SIGNAL B	:	STD_LOGIC;
   SIGNAL C	:	STD_LOGIC;
   SIGNAL D	:	STD_LOGIC;
   SIGNAL Z	:	STD_LOGIC;
   SIGNAL Y	:	STD_LOGIC;
   SIGNAL X	:	STD_LOGIC;
   SIGNAL W	:	STD_LOGIC;

BEGIN

   UUT: schema PORT MAP(
		A => A, 
		B => B, 
		C => C, 
		D => D, 
		Z => Z, 
		Y => Y, 
		X => X, 
		W => W
   );


A <= '0', '0' after 100 ns, '0' after 200 ns, '0' after 300 ns, '0' after 400 ns, '0' after 500 ns, '0' after 600 ns, '1' after 700 ns, '1' after 800 ns;
B <= '0', '0' after 100 ns, '0' after 200 ns, '0' after 300 ns, '1' after 400 ns, '1' after 500 ns, '1' after 600 ns, '0' after 700 ns, '0' after 800 ns;
C <= '0', '0' after 100 ns, '1' after 200 ns, '1' after 300 ns, '0' after 400 ns, '0' after 500 ns, '1' after 600 ns, '0' after 700 ns, '0' after 800 ns;
D <= '0', '1' after 100 ns, '0' after 200 ns, '1' after 300 ns, '0' after 400 ns, '1' after 500 ns, '1' after 600 ns, '0' after 700 ns, '1' after 800 ns;


END;
