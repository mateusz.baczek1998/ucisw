<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="A" />
        <signal name="C" />
        <signal name="Z" />
        <signal name="XLXN_29" />
        <signal name="D" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_40" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="X" />
        <signal name="XLXN_45" />
        <signal name="W" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="XLXN_50" />
        <signal name="XLXN_51" />
        <signal name="XLXN_52" />
        <signal name="XLXN_53" />
        <signal name="XLXN_54" />
        <signal name="XLXN_55" />
        <signal name="Y" />
        <signal name="B" />
        <signal name="XLXN_58" />
        <signal name="XLXN_59" />
        <port polarity="Input" name="A" />
        <port polarity="Input" name="C" />
        <port polarity="Output" name="Z" />
        <port polarity="Input" name="D" />
        <port polarity="Output" name="X" />
        <port polarity="Output" name="W" />
        <port polarity="Output" name="Y" />
        <port polarity="Input" name="B" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="and3" name="XLXI_4">
            <blockpin signalname="XLXN_4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="B" name="I2" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_5">
            <blockpin signalname="D" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="XLXN_5" name="I2" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_8">
            <blockpin signalname="XLXN_9" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_7" name="I2" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_9">
            <blockpin signalname="D" name="I0" />
            <blockpin signalname="B" name="I1" />
            <blockpin signalname="XLXN_11" name="I2" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_10">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="B" name="I1" />
            <blockpin signalname="XLXN_12" name="I2" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_11">
            <blockpin signalname="XLXN_19" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="A" name="I2" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="XLXN_20" name="I0" />
            <blockpin signalname="A" name="I1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_13">
            <blockpin signalname="XLXN_16" name="I0" />
            <blockpin signalname="XLXN_15" name="I1" />
            <blockpin signalname="XLXN_14" name="I2" />
            <blockpin signalname="XLXN_13" name="I3" />
            <blockpin signalname="XLXN_45" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_14">
            <blockpin signalname="C" name="I" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_15">
            <blockpin signalname="D" name="I" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_16">
            <blockpin signalname="B" name="I" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_17">
            <blockpin signalname="C" name="I" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_18">
            <blockpin signalname="B" name="I" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_19">
            <blockpin signalname="A" name="I" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_20">
            <blockpin signalname="A" name="I" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_21">
            <blockpin signalname="C" name="I" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_22">
            <blockpin signalname="D" name="I" />
            <blockpin signalname="XLXN_19" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_23">
            <blockpin signalname="B" name="I" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_24">
            <blockpin signalname="D" name="I" />
            <blockpin signalname="XLXN_36" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_26">
            <blockpin signalname="XLXN_36" name="I" />
            <blockpin signalname="Z" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_27">
            <blockpin signalname="XLXN_55" name="I" />
            <blockpin signalname="Y" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_28">
            <blockpin signalname="XLXN_43" name="I" />
            <blockpin signalname="X" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_29">
            <blockpin signalname="XLXN_45" name="I" />
            <blockpin signalname="W" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_32">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="D" name="I1" />
            <blockpin signalname="XLXN_50" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_33">
            <blockpin signalname="XLXN_53" name="I0" />
            <blockpin signalname="XLXN_52" name="I1" />
            <blockpin signalname="XLXN_51" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_34">
            <blockpin signalname="XLXN_51" name="I0" />
            <blockpin signalname="XLXN_50" name="I1" />
            <blockpin signalname="XLXN_55" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_35">
            <blockpin signalname="C" name="I" />
            <blockpin signalname="XLXN_52" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_36">
            <blockpin signalname="D" name="I" />
            <blockpin signalname="XLXN_53" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1136" y="768" name="XLXI_4" orien="R0" />
        <instance x="1136" y="960" name="XLXI_5" orien="R0" />
        <instance x="1136" y="1120" name="XLXI_7" orien="R0" />
        <instance x="1552" y="960" name="XLXI_8" orien="R0" />
        <instance x="1136" y="1392" name="XLXI_9" orien="R0" />
        <instance x="1136" y="1600" name="XLXI_10" orien="R0" />
        <instance x="1152" y="1792" name="XLXI_11" orien="R0" />
        <instance x="1152" y="1952" name="XLXI_12" orien="R0" />
        <instance x="1552" y="1744" name="XLXI_13" orien="R0" />
        <instance x="864" y="672" name="XLXI_14" orien="R0" />
        <instance x="864" y="736" name="XLXI_15" orien="R0" />
        <instance x="880" y="800" name="XLXI_16" orien="R0" />
        <instance x="880" y="864" name="XLXI_17" orien="R0" />
        <instance x="880" y="1024" name="XLXI_18" orien="R0" />
        <instance x="864" y="1232" name="XLXI_19" orien="R0" />
        <instance x="880" y="1440" name="XLXI_20" orien="R0" />
        <instance x="880" y="1696" name="XLXI_21" orien="R0" />
        <instance x="880" y="1760" name="XLXI_22" orien="R0" />
        <instance x="896" y="1920" name="XLXI_23" orien="R0" />
        <branch name="XLXN_3">
            <wire x2="1136" y1="640" y2="640" x1="1088" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1136" y1="704" y2="704" x1="1088" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1136" y1="768" y2="768" x1="1104" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1136" y1="832" y2="832" x1="1104" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1552" y1="640" y2="640" x1="1392" />
            <wire x2="1552" y1="640" y2="768" x1="1552" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1552" y1="832" y2="832" x1="1392" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="1552" y1="1024" y2="1024" x1="1392" />
            <wire x2="1552" y1="896" y2="1024" x1="1552" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="1136" y1="992" y2="992" x1="1104" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="1136" y1="1200" y2="1200" x1="1088" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="1136" y1="1408" y2="1408" x1="1104" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="1552" y1="1264" y2="1264" x1="1392" />
            <wire x2="1552" y1="1264" y2="1488" x1="1552" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="1472" y1="1472" y2="1472" x1="1392" />
            <wire x2="1472" y1="1472" y2="1552" x1="1472" />
            <wire x2="1552" y1="1552" y2="1552" x1="1472" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="1472" y1="1664" y2="1664" x1="1408" />
            <wire x2="1472" y1="1616" y2="1664" x1="1472" />
            <wire x2="1552" y1="1616" y2="1616" x1="1472" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="1552" y1="1856" y2="1856" x1="1408" />
            <wire x2="1552" y1="1680" y2="1856" x1="1552" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="1152" y1="1664" y2="1664" x1="1104" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="1152" y1="1728" y2="1728" x1="1104" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="1152" y1="1888" y2="1888" x1="1120" />
        </branch>
        <branch name="A">
            <wire x2="96" y1="144" y2="144" x1="80" />
            <wire x2="96" y1="144" y2="1200" x1="96" />
            <wire x2="864" y1="1200" y2="1200" x1="96" />
            <wire x2="96" y1="1200" y2="1376" x1="96" />
            <wire x2="480" y1="1376" y2="1376" x1="96" />
            <wire x2="480" y1="1376" y2="1408" x1="480" />
            <wire x2="880" y1="1408" y2="1408" x1="480" />
            <wire x2="96" y1="1376" y2="1600" x1="96" />
            <wire x2="1152" y1="1600" y2="1600" x1="96" />
            <wire x2="96" y1="1600" y2="1824" x1="96" />
            <wire x2="96" y1="1824" y2="1888" x1="96" />
            <wire x2="1152" y1="1824" y2="1824" x1="96" />
        </branch>
        <branch name="C">
            <wire x2="304" y1="1888" y2="1888" x1="288" />
            <wire x2="304" y1="144" y2="296" x1="304" />
            <wire x2="304" y1="296" y2="304" x1="304" />
            <wire x2="304" y1="304" y2="416" x1="304" />
            <wire x2="304" y1="416" y2="640" x1="304" />
            <wire x2="864" y1="640" y2="640" x1="304" />
            <wire x2="304" y1="640" y2="832" x1="304" />
            <wire x2="880" y1="832" y2="832" x1="304" />
            <wire x2="304" y1="832" y2="1056" x1="304" />
            <wire x2="1136" y1="1056" y2="1056" x1="304" />
            <wire x2="304" y1="1056" y2="1536" x1="304" />
            <wire x2="1136" y1="1536" y2="1536" x1="304" />
            <wire x2="304" y1="1536" y2="1664" x1="304" />
            <wire x2="304" y1="1664" y2="1888" x1="304" />
            <wire x2="880" y1="1664" y2="1664" x1="304" />
            <wire x2="848" y1="416" y2="416" x1="304" />
            <wire x2="848" y1="368" y2="416" x1="848" />
            <wire x2="944" y1="368" y2="368" x1="848" />
            <wire x2="1392" y1="368" y2="368" x1="944" />
            <wire x2="944" y1="352" y2="352" x1="864" />
            <wire x2="944" y1="352" y2="368" x1="944" />
            <wire x2="864" y1="352" y2="448" x1="864" />
            <wire x2="944" y1="448" y2="448" x1="864" />
        </branch>
        <iomarker fontsize="28" x="80" y="144" name="A" orien="R270" />
        <iomarker fontsize="28" x="192" y="144" name="B" orien="R270" />
        <iomarker fontsize="28" x="304" y="144" name="C" orien="R270" />
        <iomarker fontsize="28" x="416" y="144" name="D" orien="R270" />
        <branch name="Z">
            <wire x2="1312" y1="144" y2="144" x1="1296" />
            <wire x2="1312" y1="144" y2="192" x1="1312" />
            <wire x2="1328" y1="192" y2="192" x1="1312" />
            <wire x2="1344" y1="192" y2="192" x1="1328" />
        </branch>
        <iomarker fontsize="28" x="1344" y="192" name="Z" orien="R0" />
        <branch name="D">
            <wire x2="416" y1="1888" y2="1888" x1="400" />
            <wire x2="416" y1="144" y2="224" x1="416" />
            <wire x2="704" y1="224" y2="224" x1="416" />
            <wire x2="752" y1="224" y2="224" x1="704" />
            <wire x2="416" y1="224" y2="304" x1="416" />
            <wire x2="416" y1="304" y2="512" x1="416" />
            <wire x2="416" y1="512" y2="704" x1="416" />
            <wire x2="864" y1="704" y2="704" x1="416" />
            <wire x2="416" y1="704" y2="896" x1="416" />
            <wire x2="1136" y1="896" y2="896" x1="416" />
            <wire x2="416" y1="896" y2="1328" x1="416" />
            <wire x2="1136" y1="1328" y2="1328" x1="416" />
            <wire x2="416" y1="1328" y2="1728" x1="416" />
            <wire x2="416" y1="1728" y2="1888" x1="416" />
            <wire x2="880" y1="1728" y2="1728" x1="416" />
            <wire x2="832" y1="512" y2="512" x1="416" />
            <wire x2="976" y1="512" y2="512" x1="832" />
            <wire x2="1392" y1="304" y2="304" x1="416" />
            <wire x2="752" y1="192" y2="224" x1="752" />
            <wire x2="896" y1="192" y2="192" x1="752" />
            <wire x2="1088" y1="192" y2="192" x1="896" />
            <wire x2="1088" y1="192" y2="224" x1="1088" />
            <wire x2="976" y1="64" y2="64" x1="912" />
            <wire x2="912" y1="64" y2="224" x1="912" />
            <wire x2="1088" y1="224" y2="224" x1="912" />
        </branch>
        <instance x="976" y="96" name="XLXI_24" orien="R0" />
        <instance x="1072" y="176" name="XLXI_26" orien="R0" />
        <branch name="XLXN_36">
            <wire x2="1072" y1="144" y2="144" x1="992" />
            <wire x2="992" y1="144" y2="272" x1="992" />
            <wire x2="1472" y1="272" y2="272" x1="992" />
            <wire x2="1472" y1="64" y2="64" x1="1200" />
            <wire x2="1472" y1="64" y2="160" x1="1472" />
            <wire x2="1472" y1="160" y2="224" x1="1472" />
            <wire x2="1472" y1="224" y2="272" x1="1472" />
        </branch>
        <instance x="1840" y="784" name="XLXI_28" orien="R0" />
        <iomarker fontsize="28" x="2272" y="832" name="X" orien="R0" />
        <branch name="XLXN_43">
            <wire x2="1824" y1="832" y2="832" x1="1808" />
            <wire x2="1824" y1="752" y2="832" x1="1824" />
            <wire x2="1840" y1="752" y2="752" x1="1824" />
        </branch>
        <branch name="X">
            <wire x2="2160" y1="752" y2="752" x1="2064" />
            <wire x2="2160" y1="752" y2="832" x1="2160" />
            <wire x2="2272" y1="832" y2="832" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2192" y="1584" name="W" orien="R0" />
        <instance x="1872" y="1488" name="XLXI_29" orien="R0" />
        <branch name="XLXN_45">
            <wire x2="1840" y1="1584" y2="1584" x1="1808" />
            <wire x2="1840" y1="1456" y2="1584" x1="1840" />
            <wire x2="1872" y1="1456" y2="1456" x1="1840" />
        </branch>
        <branch name="W">
            <wire x2="2144" y1="1456" y2="1456" x1="2096" />
            <wire x2="2144" y1="1456" y2="1584" x1="2144" />
            <wire x2="2192" y1="1584" y2="1584" x1="2144" />
        </branch>
        <iomarker fontsize="28" x="2320" y="384" name="Y" orien="R0" />
        <instance x="2016" y="416" name="XLXI_27" orien="R0" />
        <instance x="976" y="544" name="XLXI_36" orien="R0" />
        <instance x="944" y="480" name="XLXI_35" orien="R0" />
        <instance x="1728" y="496" name="XLXI_34" orien="R0" />
        <instance x="1392" y="560" name="XLXI_33" orien="R0" />
        <instance x="1392" y="432" name="XLXI_32" orien="R0" />
        <branch name="XLXN_50">
            <wire x2="1680" y1="336" y2="336" x1="1648" />
            <wire x2="1680" y1="336" y2="368" x1="1680" />
            <wire x2="1728" y1="368" y2="368" x1="1680" />
        </branch>
        <branch name="XLXN_51">
            <wire x2="1680" y1="464" y2="464" x1="1648" />
            <wire x2="1680" y1="432" y2="464" x1="1680" />
            <wire x2="1728" y1="432" y2="432" x1="1680" />
        </branch>
        <branch name="XLXN_52">
            <wire x2="1280" y1="448" y2="448" x1="1168" />
            <wire x2="1280" y1="432" y2="448" x1="1280" />
            <wire x2="1392" y1="432" y2="432" x1="1280" />
        </branch>
        <branch name="XLXN_53">
            <wire x2="1296" y1="512" y2="512" x1="1200" />
            <wire x2="1296" y1="496" y2="512" x1="1296" />
            <wire x2="1392" y1="496" y2="496" x1="1296" />
        </branch>
        <branch name="XLXN_55">
            <wire x2="2000" y1="400" y2="400" x1="1984" />
            <wire x2="2000" y1="384" y2="400" x1="2000" />
            <wire x2="2016" y1="384" y2="384" x1="2000" />
        </branch>
        <branch name="Y">
            <wire x2="2320" y1="384" y2="384" x1="2240" />
        </branch>
        <branch name="B">
            <wire x2="192" y1="144" y2="576" x1="192" />
            <wire x2="192" y1="576" y2="768" x1="192" />
            <wire x2="192" y1="768" y2="976" x1="192" />
            <wire x2="192" y1="976" y2="1264" x1="192" />
            <wire x2="192" y1="1264" y2="1272" x1="192" />
            <wire x2="192" y1="1272" y2="1472" x1="192" />
            <wire x2="192" y1="1472" y2="1872" x1="192" />
            <wire x2="192" y1="1872" y2="1904" x1="192" />
            <wire x2="544" y1="1872" y2="1872" x1="192" />
            <wire x2="544" y1="1872" y2="1888" x1="544" />
            <wire x2="896" y1="1888" y2="1888" x1="544" />
            <wire x2="1136" y1="1472" y2="1472" x1="192" />
            <wire x2="1136" y1="1264" y2="1264" x1="192" />
            <wire x2="832" y1="976" y2="976" x1="192" />
            <wire x2="832" y1="976" y2="992" x1="832" />
            <wire x2="864" y1="992" y2="992" x1="832" />
            <wire x2="880" y1="992" y2="992" x1="864" />
            <wire x2="880" y1="768" y2="768" x1="192" />
            <wire x2="1136" y1="576" y2="576" x1="192" />
        </branch>
    </sheet>
</drawing>