/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/wint3rmute/code/ucisw/zly_detektor_sekw/test.vhd";



static void work_a_0059012754_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    int64 t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int64 t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    int64 t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    int64 t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    int64 t37;
    char *t38;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    int64 t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    int64 t49;
    char *t50;
    char *t51;
    char *t52;
    char *t53;
    char *t54;
    int64 t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    char *t60;
    int64 t61;
    char *t62;
    char *t63;
    char *t64;
    char *t65;
    char *t66;
    int64 t67;
    char *t68;
    char *t69;
    char *t70;
    char *t71;
    char *t72;
    int64 t73;
    char *t74;
    char *t75;
    char *t76;
    char *t77;
    char *t78;
    int64 t79;
    char *t80;
    char *t81;
    char *t82;
    char *t83;
    char *t84;
    int64 t85;
    char *t86;
    char *t87;
    char *t88;
    char *t89;
    char *t90;
    int64 t91;
    char *t92;
    char *t93;
    char *t94;
    char *t95;
    char *t96;

LAB0:    t1 = (t0 + 2984U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(55, ng0);
    t2 = (t0 + 3368);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_delta(t2, 0U, 1, 0LL);
    t7 = (100 * 1000LL);
    t8 = (t0 + 3368);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t8, 0U, 1, t7);
    t13 = (200 * 1000LL);
    t14 = (t0 + 3368);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    *((unsigned char *)t18) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t14, 0U, 1, t13);
    t19 = (300 * 1000LL);
    t20 = (t0 + 3368);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t20, 0U, 1, t19);
    t25 = (400 * 1000LL);
    t26 = (t0 + 3368);
    t27 = (t26 + 56U);
    t28 = *((char **)t27);
    t29 = (t28 + 56U);
    t30 = *((char **)t29);
    *((unsigned char *)t30) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t26, 0U, 1, t25);
    t31 = (500 * 1000LL);
    t32 = (t0 + 3368);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    t35 = (t34 + 56U);
    t36 = *((char **)t35);
    *((unsigned char *)t36) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t32, 0U, 1, t31);
    t37 = (600 * 1000LL);
    t38 = (t0 + 3368);
    t39 = (t38 + 56U);
    t40 = *((char **)t39);
    t41 = (t40 + 56U);
    t42 = *((char **)t41);
    *((unsigned char *)t42) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t38, 0U, 1, t37);
    t43 = (700 * 1000LL);
    t44 = (t0 + 3368);
    t45 = (t44 + 56U);
    t46 = *((char **)t45);
    t47 = (t46 + 56U);
    t48 = *((char **)t47);
    *((unsigned char *)t48) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t44, 0U, 1, t43);
    t49 = (800 * 1000LL);
    t50 = (t0 + 3368);
    t51 = (t50 + 56U);
    t52 = *((char **)t51);
    t53 = (t52 + 56U);
    t54 = *((char **)t53);
    *((unsigned char *)t54) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t50, 0U, 1, t49);
    t55 = (900 * 1000LL);
    t56 = (t0 + 3368);
    t57 = (t56 + 56U);
    t58 = *((char **)t57);
    t59 = (t58 + 56U);
    t60 = *((char **)t59);
    *((unsigned char *)t60) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t56, 0U, 1, t55);
    t61 = (1000 * 1000LL);
    t62 = (t0 + 3368);
    t63 = (t62 + 56U);
    t64 = *((char **)t63);
    t65 = (t64 + 56U);
    t66 = *((char **)t65);
    *((unsigned char *)t66) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t62, 0U, 1, t61);
    t67 = (1100 * 1000LL);
    t68 = (t0 + 3368);
    t69 = (t68 + 56U);
    t70 = *((char **)t69);
    t71 = (t70 + 56U);
    t72 = *((char **)t71);
    *((unsigned char *)t72) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t68, 0U, 1, t67);
    t73 = (1200 * 1000LL);
    t74 = (t0 + 3368);
    t75 = (t74 + 56U);
    t76 = *((char **)t75);
    t77 = (t76 + 56U);
    t78 = *((char **)t77);
    *((unsigned char *)t78) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t74, 0U, 1, t73);
    t79 = (1300 * 1000LL);
    t80 = (t0 + 3368);
    t81 = (t80 + 56U);
    t82 = *((char **)t81);
    t83 = (t82 + 56U);
    t84 = *((char **)t83);
    *((unsigned char *)t84) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t80, 0U, 1, t79);
    t85 = (1400 * 1000LL);
    t86 = (t0 + 3368);
    t87 = (t86 + 56U);
    t88 = *((char **)t87);
    t89 = (t88 + 56U);
    t90 = *((char **)t89);
    *((unsigned char *)t90) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t86, 0U, 1, t85);
    t91 = (1500 * 1000LL);
    t92 = (t0 + 3368);
    t93 = (t92 + 56U);
    t94 = *((char **)t93);
    t95 = (t94 + 56U);
    t96 = *((char **)t95);
    *((unsigned char *)t96) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t92, 0U, 1, t91);
    xsi_set_current_line(57, ng0);
    t2 = (t0 + 3432);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_delta(t2, 0U, 1, 0LL);
    t7 = (100 * 1000LL);
    t8 = (t0 + 3432);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t8, 0U, 1, t7);
    t13 = (200 * 1000LL);
    t14 = (t0 + 3432);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    *((unsigned char *)t18) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t14, 0U, 1, t13);
    t19 = (300 * 1000LL);
    t20 = (t0 + 3432);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t20, 0U, 1, t19);
    t25 = (400 * 1000LL);
    t26 = (t0 + 3432);
    t27 = (t26 + 56U);
    t28 = *((char **)t27);
    t29 = (t28 + 56U);
    t30 = *((char **)t29);
    *((unsigned char *)t30) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t26, 0U, 1, t25);
    t31 = (500 * 1000LL);
    t32 = (t0 + 3432);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    t35 = (t34 + 56U);
    t36 = *((char **)t35);
    *((unsigned char *)t36) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t32, 0U, 1, t31);
    t37 = (600 * 1000LL);
    t38 = (t0 + 3432);
    t39 = (t38 + 56U);
    t40 = *((char **)t39);
    t41 = (t40 + 56U);
    t42 = *((char **)t41);
    *((unsigned char *)t42) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t38, 0U, 1, t37);
    t43 = (700 * 1000LL);
    t44 = (t0 + 3432);
    t45 = (t44 + 56U);
    t46 = *((char **)t45);
    t47 = (t46 + 56U);
    t48 = *((char **)t47);
    *((unsigned char *)t48) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t44, 0U, 1, t43);
    t49 = (800 * 1000LL);
    t50 = (t0 + 3432);
    t51 = (t50 + 56U);
    t52 = *((char **)t51);
    t53 = (t52 + 56U);
    t54 = *((char **)t53);
    *((unsigned char *)t54) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t50, 0U, 1, t49);
    t55 = (900 * 1000LL);
    t56 = (t0 + 3432);
    t57 = (t56 + 56U);
    t58 = *((char **)t57);
    t59 = (t58 + 56U);
    t60 = *((char **)t59);
    *((unsigned char *)t60) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t56, 0U, 1, t55);
    t61 = (1000 * 1000LL);
    t62 = (t0 + 3432);
    t63 = (t62 + 56U);
    t64 = *((char **)t63);
    t65 = (t64 + 56U);
    t66 = *((char **)t65);
    *((unsigned char *)t66) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t62, 0U, 1, t61);
    t67 = (1100 * 1000LL);
    t68 = (t0 + 3432);
    t69 = (t68 + 56U);
    t70 = *((char **)t69);
    t71 = (t70 + 56U);
    t72 = *((char **)t71);
    *((unsigned char *)t72) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t68, 0U, 1, t67);
    t73 = (1200 * 1000LL);
    t74 = (t0 + 3432);
    t75 = (t74 + 56U);
    t76 = *((char **)t75);
    t77 = (t76 + 56U);
    t78 = *((char **)t77);
    *((unsigned char *)t78) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t74, 0U, 1, t73);
    t79 = (1300 * 1000LL);
    t80 = (t0 + 3432);
    t81 = (t80 + 56U);
    t82 = *((char **)t81);
    t83 = (t82 + 56U);
    t84 = *((char **)t83);
    *((unsigned char *)t84) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t80, 0U, 1, t79);
    t85 = (1400 * 1000LL);
    t86 = (t0 + 3432);
    t87 = (t86 + 56U);
    t88 = *((char **)t87);
    t89 = (t88 + 56U);
    t90 = *((char **)t89);
    *((unsigned char *)t90) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t86, 0U, 1, t85);
    t91 = (1500 * 1000LL);
    t92 = (t0 + 3432);
    t93 = (t92 + 56U);
    t94 = *((char **)t93);
    t95 = (t94 + 56U);
    t96 = *((char **)t95);
    *((unsigned char *)t96) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t92, 0U, 1, t91);
    xsi_set_current_line(58, ng0);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

}


extern void work_a_0059012754_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0059012754_3212880686_p_0};
	xsi_register_didat("work_a_0059012754_3212880686", "isim/detektron_detektron_sch_tb_isim_beh.exe.sim/work/a_0059012754_3212880686.didat");
	xsi_register_executes(pe);
}
