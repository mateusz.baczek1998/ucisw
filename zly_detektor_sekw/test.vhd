-- Vhdl test bench created from schematic /home/wint3rmute/code/ucisw/detektor_sekw/detektron.sch - Tue Nov 12 12:34:49 2019
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY detektron_detektron_sch_tb IS
END detektron_detektron_sch_tb;
ARCHITECTURE behavioral OF detektron_detektron_sch_tb IS 

   COMPONENT detektron
   PORT( Q0	:	OUT	STD_LOGIC; 
          Q2	:	OUT	STD_LOGIC; 
          Zegar	:	IN	STD_LOGIC; 
          Z	:	IN	STD_LOGIC; 
          Q1	:	OUT	STD_LOGIC; 
          Y	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL Q0	:	STD_LOGIC;
   SIGNAL Q2	:	STD_LOGIC;
   SIGNAL Zegar	:	STD_LOGIC;
   SIGNAL Z	:	STD_LOGIC;
   SIGNAL Q1	:	STD_LOGIC;
   SIGNAL Y	:	STD_LOGIC;

BEGIN

   UUT: detektron PORT MAP(
		Q0 => Q0, 
		Q2 => Q2, 
		Zegar => Zegar, 
		Z => Z, 
		Q1 => Q1, 
		Y => Y
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	
		Z <= '0', '0' after 100ns, '0' after 200 ns, '0' after 300 ns, '0' after 400 ns, '0' after 500 ns, '0' after 600 ns, '1' after 700 ns, '0' after 800 ns, '1' after 900 ns, '0' after 1000 ns, '1' after 1100 ns, '0' after 1200 ns, '1' after 1300 ns, '0' after 1400 ns, '1' after 1500 ns;
      
		Zegar <= '0', '1' after 100ns, '0' after 200 ns, '1' after 300 ns, '0' after 400 ns, '1' after 500 ns, '0' after 600 ns, '1' after 700 ns, '0' after 800 ns, '1' after 900 ns, '0' after 1000 ns, '1' after 1100 ns, '0' after 1200 ns, '1' after 1300 ns, '0' after 1400 ns, '1' after 1500 ns;
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
