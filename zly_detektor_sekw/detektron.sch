<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_16" />
        <signal name="Q0" />
        <signal name="XLXN_18" />
        <signal name="Q2" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_27" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="Zegar" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="Z" />
        <signal name="XLXN_46" />
        <signal name="XLXN_48" />
        <signal name="XLXN_50" />
        <signal name="XLXN_51" />
        <signal name="XLXN_52" />
        <signal name="XLXN_53" />
        <signal name="XLXN_54" />
        <signal name="XLXN_55" />
        <signal name="XLXN_56" />
        <signal name="XLXN_58" />
        <signal name="Q1" />
        <signal name="XLXN_61" />
        <signal name="XLXN_62" />
        <signal name="Y" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="Zegar" />
        <port polarity="Input" name="Z" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Y" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <block symbolname="inv" name="XLXI_7">
            <blockpin signalname="Q0" name="I" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="Q1" name="I" />
            <blockpin signalname="Q1" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_9">
            <blockpin signalname="Q2" name="I" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_10">
            <blockpin signalname="XLXN_43" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_11">
            <blockpin signalname="Q0" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="XLXN_24" name="I2" />
            <blockpin signalname="Z" name="I3" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_12">
            <blockpin signalname="XLXN_21" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="Q2" name="I2" />
            <blockpin signalname="Z" name="I3" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_1">
            <blockpin signalname="Zegar" name="C" />
            <blockpin signalname="XLXN_27" name="D" />
            <blockpin signalname="Q0" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_2">
            <blockpin signalname="Zegar" name="C" />
            <blockpin signalname="XLXN_48" name="D" />
            <blockpin signalname="Q1" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <blockpin signalname="Zegar" name="C" />
            <blockpin signalname="XLXN_56" name="D" />
            <blockpin signalname="Q2" name="Q" />
        </block>
        <block symbolname="or3" name="XLXI_22">
            <blockpin signalname="XLXN_50" name="I0" />
            <blockpin signalname="XLXN_51" name="I1" />
            <blockpin signalname="XLXN_52" name="I2" />
            <blockpin signalname="XLXN_48" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_24">
            <blockpin signalname="XLXN_24" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="Q0" name="I2" />
            <blockpin signalname="Q1" name="I3" />
            <blockpin signalname="XLXN_51" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_25">
            <blockpin signalname="Q2" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="XLXN_21" name="I2" />
            <blockpin signalname="XLXN_52" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_26">
            <blockpin signalname="XLXN_53" name="I0" />
            <blockpin signalname="XLXN_54" name="I1" />
            <blockpin signalname="XLXN_55" name="I2" />
            <blockpin signalname="XLXN_56" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_27">
            <blockpin signalname="XLXN_21" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="Z" name="I2" />
            <blockpin signalname="XLXN_53" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_28">
            <blockpin signalname="XLXN_21" name="I0" />
            <blockpin signalname="XLXN_24" name="I1" />
            <blockpin signalname="Z" name="I2" />
            <blockpin signalname="XLXN_54" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_29">
            <blockpin signalname="Q0" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="Z" name="I2" />
            <blockpin signalname="XLXN_55" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_31">
            <blockpin signalname="Z" name="I" />
            <blockpin signalname="Q1" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_33">
            <blockpin signalname="Q2" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="Q0" name="I2" />
            <blockpin signalname="Y" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_34">
            <blockpin signalname="XLXN_24" name="I0" />
            <blockpin signalname="Z" name="I1" />
            <blockpin signalname="XLXN_21" name="I2" />
            <blockpin signalname="Q1" name="I3" />
            <blockpin signalname="XLXN_50" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1088" y="320" name="XLXI_7" orien="R90" />
        <instance x="1968" y="320" name="XLXI_8" orien="R90" />
        <instance x="2848" y="320" name="XLXI_9" orien="R90" />
        <branch name="Q0">
            <wire x2="528" y1="560" y2="560" x1="432" />
            <wire x2="1168" y1="560" y2="560" x1="528" />
            <wire x2="1200" y1="560" y2="560" x1="1168" />
            <wire x2="1200" y1="560" y2="1488" x1="1200" />
            <wire x2="1664" y1="560" y2="560" x1="1200" />
            <wire x2="1664" y1="560" y2="880" x1="1664" />
            <wire x2="2672" y1="560" y2="560" x1="1664" />
            <wire x2="2672" y1="560" y2="944" x1="2672" />
            <wire x2="3360" y1="560" y2="560" x1="2672" />
            <wire x2="3360" y1="560" y2="800" x1="3360" />
            <wire x2="528" y1="560" y2="800" x1="528" />
            <wire x2="1120" y1="304" y2="320" x1="1120" />
            <wire x2="1168" y1="304" y2="304" x1="1120" />
            <wire x2="1168" y1="304" y2="560" x1="1168" />
            <wire x2="1168" y1="288" y2="304" x1="1168" />
            <wire x2="1200" y1="1488" y2="1488" x1="1184" />
        </branch>
        <branch name="Q2">
            <wire x2="896" y1="656" y2="656" x1="432" />
            <wire x2="896" y1="656" y2="800" x1="896" />
            <wire x2="1776" y1="656" y2="656" x1="896" />
            <wire x2="1776" y1="656" y2="880" x1="1776" />
            <wire x2="2928" y1="656" y2="656" x1="1776" />
            <wire x2="2960" y1="656" y2="656" x1="2928" />
            <wire x2="2960" y1="656" y2="1488" x1="2960" />
            <wire x2="3232" y1="656" y2="656" x1="2960" />
            <wire x2="3232" y1="656" y2="800" x1="3232" />
            <wire x2="2880" y1="304" y2="320" x1="2880" />
            <wire x2="2928" y1="304" y2="304" x1="2880" />
            <wire x2="2928" y1="304" y2="656" x1="2928" />
            <wire x2="2928" y1="288" y2="304" x1="2928" />
            <wire x2="2960" y1="1488" y2="1488" x1="2944" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="768" y1="576" y2="576" x1="432" />
            <wire x2="1120" y1="576" y2="576" x1="768" />
            <wire x2="1408" y1="576" y2="576" x1="1120" />
            <wire x2="1408" y1="576" y2="880" x1="1408" />
            <wire x2="1904" y1="576" y2="576" x1="1408" />
            <wire x2="1904" y1="576" y2="880" x1="1904" />
            <wire x2="2192" y1="576" y2="576" x1="1904" />
            <wire x2="2192" y1="576" y2="944" x1="2192" />
            <wire x2="2432" y1="576" y2="576" x1="2192" />
            <wire x2="2432" y1="576" y2="944" x1="2432" />
            <wire x2="3120" y1="576" y2="576" x1="2432" />
            <wire x2="768" y1="576" y2="800" x1="768" />
            <wire x2="1120" y1="544" y2="576" x1="1120" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="656" y1="672" y2="672" x1="432" />
            <wire x2="656" y1="672" y2="800" x1="656" />
            <wire x2="1280" y1="672" y2="672" x1="656" />
            <wire x2="1280" y1="672" y2="880" x1="1280" />
            <wire x2="1536" y1="672" y2="672" x1="1280" />
            <wire x2="1536" y1="672" y2="880" x1="1536" />
            <wire x2="2496" y1="672" y2="672" x1="1536" />
            <wire x2="2496" y1="672" y2="944" x1="2496" />
            <wire x2="2880" y1="672" y2="672" x1="2496" />
            <wire x2="3120" y1="672" y2="672" x1="2880" />
            <wire x2="2880" y1="544" y2="672" x1="2880" />
        </branch>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1040" y="620">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1040" y="668">Q_2</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1040" y="572">Q_0</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1184" y="620">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1184" y="668">Q_2</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1184" y="572">Q_0</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1920" y="620">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1920" y="668">Q_2</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1920" y="572">Q_0</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2064" y="620">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2064" y="668">Q_2</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2064" y="572">Q_0</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2800" y="620">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2800" y="668">Q_2</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2800" y="572">Q_0</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2960" y="620">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2960" y="668">Q_2</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2960" y="572">Q_0</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="3152" y="620">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="3152" y="668">Q_2</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="3152" y="572">Q_0</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="336" y="620">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="336" y="668">Q_2</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="336" y="572">Q_0</text>
        <branch name="XLXN_27">
            <wire x2="752" y1="1360" y2="1488" x1="752" />
            <wire x2="800" y1="1488" y2="1488" x1="752" />
        </branch>
        <instance x="800" y="1744" name="XLXI_1" orien="R0" />
        <instance x="1680" y="1744" name="XLXI_2" orien="R0" />
        <instance x="2560" y="1744" name="XLXI_3" orien="R0" />
        <branch name="Zegar">
            <wire x2="784" y1="1728" y2="1728" x1="560" />
            <wire x2="1664" y1="1728" y2="1728" x1="784" />
            <wire x2="2544" y1="1728" y2="1728" x1="1664" />
            <wire x2="2640" y1="1728" y2="1728" x1="2544" />
            <wire x2="800" y1="1616" y2="1616" x1="784" />
            <wire x2="784" y1="1616" y2="1728" x1="784" />
            <wire x2="1680" y1="1616" y2="1616" x1="1664" />
            <wire x2="1664" y1="1616" y2="1728" x1="1664" />
            <wire x2="2560" y1="1616" y2="1616" x1="2544" />
            <wire x2="2544" y1="1616" y2="1728" x1="2544" />
        </branch>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="956" y="1564">Q_0</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="1820" y="1564">Q_1</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(255,0,255)" x="2700" y="1580">Q_2</text>
        <instance x="656" y="1104" name="XLXI_10" orien="R90" />
        <instance x="464" y="800" name="XLXI_11" orien="R90" />
        <instance x="704" y="800" name="XLXI_12" orien="R90" />
        <branch name="XLXN_43">
            <wire x2="624" y1="1056" y2="1072" x1="624" />
            <wire x2="720" y1="1072" y2="1072" x1="624" />
            <wire x2="720" y1="1072" y2="1104" x1="720" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="784" y1="1072" y2="1104" x1="784" />
            <wire x2="864" y1="1072" y2="1072" x1="784" />
            <wire x2="864" y1="1056" y2="1072" x1="864" />
        </branch>
        <branch name="Z">
            <wire x2="144" y1="800" y2="848" x1="144" />
            <wire x2="224" y1="848" y2="848" x1="144" />
            <wire x2="240" y1="800" y2="800" x1="144" />
            <wire x2="240" y1="752" y2="752" x1="224" />
            <wire x2="240" y1="752" y2="800" x1="240" />
            <wire x2="416" y1="752" y2="752" x1="240" />
            <wire x2="416" y1="704" y2="752" x1="416" />
            <wire x2="720" y1="704" y2="704" x1="416" />
            <wire x2="720" y1="704" y2="800" x1="720" />
            <wire x2="960" y1="704" y2="704" x1="720" />
            <wire x2="960" y1="704" y2="800" x1="960" />
            <wire x2="1344" y1="704" y2="704" x1="960" />
            <wire x2="1344" y1="704" y2="880" x1="1344" />
            <wire x2="2320" y1="704" y2="704" x1="1344" />
            <wire x2="2560" y1="704" y2="704" x1="2320" />
            <wire x2="2560" y1="704" y2="944" x1="2560" />
            <wire x2="2800" y1="704" y2="704" x1="2560" />
            <wire x2="3120" y1="704" y2="704" x1="2800" />
            <wire x2="2800" y1="704" y2="944" x1="2800" />
            <wire x2="2320" y1="704" y2="944" x1="2320" />
        </branch>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(128,0,128)" x="3136" y="716">Z</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(128,0,128)" x="2976" y="716">Z</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(128,0,128)" x="2832" y="716">Z</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(128,0,128)" x="1072" y="716">Z</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(128,0,128)" x="1216" y="716">Z</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(128,0,128)" x="1952" y="716">Z</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(128,0,128)" x="2096" y="716">Z</text>
        <text style="fontsize:40;fontname:Arial;textcolor:rgb(128,0,128)" x="384" y="716">Z</text>
        <instance x="1488" y="1216" name="XLXI_22" orien="R90" />
        <instance x="1472" y="880" name="XLXI_24" orien="R90" />
        <instance x="1712" y="880" name="XLXI_25" orien="R90" />
        <branch name="XLXN_48">
            <wire x2="1616" y1="1472" y2="1488" x1="1616" />
            <wire x2="1680" y1="1488" y2="1488" x1="1616" />
        </branch>
        <branch name="XLXN_50">
            <wire x2="1376" y1="1136" y2="1216" x1="1376" />
            <wire x2="1552" y1="1216" y2="1216" x1="1376" />
        </branch>
        <branch name="XLXN_51">
            <wire x2="1616" y1="1168" y2="1216" x1="1616" />
            <wire x2="1632" y1="1168" y2="1168" x1="1616" />
            <wire x2="1632" y1="1136" y2="1168" x1="1632" />
        </branch>
        <branch name="XLXN_52">
            <wire x2="1840" y1="1216" y2="1216" x1="1680" />
            <wire x2="1840" y1="1136" y2="1216" x1="1840" />
        </branch>
        <instance x="2128" y="944" name="XLXI_27" orien="R90" />
        <instance x="2368" y="944" name="XLXI_28" orien="R90" />
        <instance x="2608" y="944" name="XLXI_29" orien="R90" />
        <branch name="XLXN_53">
            <wire x2="2256" y1="1200" y2="1216" x1="2256" />
            <wire x2="2432" y1="1216" y2="1216" x1="2256" />
        </branch>
        <branch name="XLXN_54">
            <wire x2="2496" y1="1200" y2="1216" x1="2496" />
        </branch>
        <branch name="XLXN_55">
            <wire x2="2736" y1="1216" y2="1216" x1="2560" />
            <wire x2="2736" y1="1200" y2="1216" x1="2736" />
        </branch>
        <instance x="2368" y="1216" name="XLXI_26" orien="R90" />
        <branch name="XLXN_56">
            <wire x2="2496" y1="1472" y2="1488" x1="2496" />
            <wire x2="2560" y1="1488" y2="1488" x1="2496" />
        </branch>
        <iomarker fontsize="28" x="224" y="752" name="Z" orien="R180" />
        <instance x="224" y="880" name="XLXI_31" orien="R0" />
        <iomarker fontsize="28" x="560" y="1728" name="Zegar" orien="R180" />
        <iomarker fontsize="28" x="1168" y="288" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="2928" y="288" name="Q2" orien="R270" />
        <iomarker fontsize="28" x="2048" y="288" name="Q1" orien="R270" />
        <instance x="3168" y="800" name="XLXI_33" orien="R90" />
        <branch name="Y">
            <wire x2="3296" y1="1056" y2="1104" x1="3296" />
        </branch>
        <iomarker fontsize="28" x="3296" y="1104" name="Y" orien="R90" />
        <branch name="Q1">
            <wire x2="832" y1="608" y2="608" x1="416" />
            <wire x2="832" y1="608" y2="800" x1="832" />
            <wire x2="1472" y1="608" y2="608" x1="832" />
            <wire x2="1488" y1="608" y2="608" x1="1472" />
            <wire x2="2048" y1="608" y2="608" x1="1488" />
            <wire x2="2080" y1="608" y2="608" x1="2048" />
            <wire x2="2080" y1="608" y2="1488" x1="2080" />
            <wire x2="3120" y1="608" y2="608" x1="2080" />
            <wire x2="1472" y1="608" y2="880" x1="1472" />
            <wire x2="2000" y1="304" y2="320" x1="2000" />
            <wire x2="2048" y1="304" y2="304" x1="2000" />
            <wire x2="2048" y1="304" y2="608" x1="2048" />
            <wire x2="2048" y1="288" y2="304" x1="2048" />
            <wire x2="2080" y1="1488" y2="1488" x1="2064" />
        </branch>
        <branch name="Q1">
            <wire x2="592" y1="624" y2="624" x1="432" />
            <wire x2="592" y1="624" y2="800" x1="592" />
            <wire x2="1728" y1="624" y2="624" x1="592" />
            <wire x2="1728" y1="624" y2="880" x1="1728" />
            <wire x2="2000" y1="624" y2="624" x1="1728" />
            <wire x2="2272" y1="624" y2="624" x1="2000" />
            <wire x2="2736" y1="624" y2="624" x1="2272" />
            <wire x2="2736" y1="624" y2="944" x1="2736" />
            <wire x2="2912" y1="624" y2="624" x1="2736" />
            <wire x2="2272" y1="624" y2="720" x1="2272" />
            <wire x2="2272" y1="720" y2="784" x1="2272" />
            <wire x2="3136" y1="720" y2="720" x1="2272" />
            <wire x2="480" y1="848" y2="848" x1="448" />
            <wire x2="480" y1="720" y2="848" x1="480" />
            <wire x2="1488" y1="720" y2="720" x1="480" />
            <wire x2="1616" y1="720" y2="720" x1="1488" />
            <wire x2="1616" y1="720" y2="880" x1="1616" />
            <wire x2="1856" y1="720" y2="720" x1="1616" />
            <wire x2="2272" y1="720" y2="720" x1="1856" />
            <wire x2="1856" y1="720" y2="880" x1="1856" />
            <wire x2="1616" y1="880" y2="880" x1="1600" />
            <wire x2="1856" y1="880" y2="880" x1="1840" />
            <wire x2="2000" y1="544" y2="624" x1="2000" />
            <wire x2="2256" y1="784" y2="944" x1="2256" />
            <wire x2="2272" y1="784" y2="784" x1="2256" />
            <wire x2="2944" y1="592" y2="592" x1="2912" />
            <wire x2="2944" y1="592" y2="624" x1="2944" />
            <wire x2="3312" y1="624" y2="624" x1="2944" />
            <wire x2="3312" y1="624" y2="704" x1="3312" />
            <wire x2="2912" y1="592" y2="624" x1="2912" />
            <wire x2="3296" y1="704" y2="800" x1="3296" />
            <wire x2="3312" y1="704" y2="704" x1="3296" />
        </branch>
        <instance x="1216" y="880" name="XLXI_34" orien="R90" />
    </sheet>
</drawing>