-- Vhdl test bench created from schematic C:\Users\lab\Downloads\ucisw-master\ucisw-master\cdvhjcdbhjcdsbhjcds\schema.sch - Tue Nov 12 20:36:50 2019
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY schema_schema_sch_tb IS
END schema_schema_sch_tb;
ARCHITECTURE behavioral OF schema_schema_sch_tb IS 

   COMPONENT schema
   PORT( q1	:	OUT	STD_LOGIC; 
          q2	:	OUT	STD_LOGIC; 
          z	:	IN	STD_LOGIC; 
          q0	:	OUT	STD_LOGIC; 
          y	:	OUT	STD_LOGIC; 
          zegar	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL q1	:	STD_LOGIC;
   SIGNAL q2	:	STD_LOGIC;
   SIGNAL z	:	STD_LOGIC;
   SIGNAL q0	:	STD_LOGIC;
   SIGNAL y	:	STD_LOGIC;
   SIGNAL zegar	:	STD_LOGIC;

BEGIN

   UUT: schema PORT MAP(
		q1 => q1, 
		q2 => q2, 
		z => z, 
		q0 => q0, 
		y => y, 
		zegar => zegar
   );

		z <= '0', '1' after 90ns, '0' after 290 ns, '1' after 490 ns, '1' after 690 ns, '1' after 890 ns;
		ZEGAR <= '0', '1' after 100ns, '0' after 200 ns, '1' after 300 ns, '0' after 400 ns, '1' after 500 ns, '0' after 600 ns, '1' after 700 ns, '0' after 800 ns, '1' after 900 ns, '0' after 1000 ns, '1' after 1100 ns, '0' after 1200 ns, '1' after 1300 ns, '0' after 1400 ns, '1' after 1500 ns, '0' after 1600 ns, '1' after 1700 ns, '0' after 1800 ns, '1' after 1900 ns;


END;
