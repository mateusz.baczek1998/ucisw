--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : schema.vhf
-- /___/   /\     Timestamp : 11/25/2019 12:09:18
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl /home/wint3rmute/code/ucisw/detektor_sekwencji_rot_enc/schema.vhf -w /home/wint3rmute/code/ucisw/detektor_sekwencji_rot_enc/schema.sch
--Design Name: schema
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FD_MXILINX_schema is
   generic( INIT : bit :=  '0');
   port ( C : in    std_logic; 
          D : in    std_logic; 
          Q : out   std_logic);
end FD_MXILINX_schema;

architecture BEHAVIORAL of FD_MXILINX_schema is
   attribute BOX_TYPE   : string ;
   signal XLXN_4 : std_logic;
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
begin
   I_36_43 : GND
      port map (G=>XLXN_4);
   
   U0 : FDCP
   generic map( INIT => INIT)
      port map (C=>C,
                CLR=>XLXN_4,
                D=>D,
                PRE=>XLXN_4,
                Q=>Q);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity schema is
   port ( z     : in    std_logic; 
          zegar : in    std_logic; 
          q0    : out   std_logic; 
          q1    : out   std_logic; 
          q2    : out   std_logic; 
          y     : out   std_logic);
end schema;

architecture BEHAVIORAL of schema is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal left     : std_logic;
   signal XLXN_2   : std_logic;
   signal XLXN_4   : std_logic;
   signal XLXN_6   : std_logic;
   signal XLXN_16  : std_logic;
   signal XLXN_17  : std_logic;
   signal XLXN_18  : std_logic;
   signal XLXN_20  : std_logic;
   signal XLXN_21  : std_logic;
   signal XLXN_23  : std_logic;
   signal XLXN_24  : std_logic;
   signal XLXN_25  : std_logic;
   signal XLXN_27  : std_logic;
   signal XLXN_28  : std_logic;
   signal XLXN_29  : std_logic;
   signal q0_DUMMY : std_logic;
   signal q1_DUMMY : std_logic;
   signal q2_DUMMY : std_logic;
   component FD_MXILINX_schema
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component AND4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND4 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component OR3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR3 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_1 : label is "XLXI_1_2";
   attribute HU_SET of XLXI_2 : label is "XLXI_2_0";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_1";
begin
   q0 <= q0_DUMMY;
   q1 <= q1_DUMMY;
   q2 <= q2_DUMMY;
   XLXI_1 : FD_MXILINX_schema
      port map (C=>zegar,
                D=>XLXN_16,
                Q=>q0_DUMMY);
   
   XLXI_2 : FD_MXILINX_schema
      port map (C=>zegar,
                D=>XLXN_20,
                Q=>q1_DUMMY);
   
   XLXI_3 : FD_MXILINX_schema
      port map (C=>zegar,
                D=>XLXN_21,
                Q=>q2_DUMMY);
   
   XLXI_4 : INV
      port map (I=>q0_DUMMY,
                O=>XLXN_2);
   
   XLXI_5 : INV
      port map (I=>q1_DUMMY,
                O=>XLXN_4);
   
   XLXI_6 : INV
      port map (I=>q2_DUMMY,
                O=>XLXN_6);
   
   XLXI_14 : AND4
      port map (I0=>XLXN_2,
                I1=>q1_DUMMY,
                I2=>q2_DUMMY,
                I3=>z,
                O=>XLXN_18);
   
   XLXI_16 : AND4
      port map (I0=>q0_DUMMY,
                I1=>XLXN_4,
                I2=>XLXN_6,
                I3=>z,
                O=>XLXN_17);
   
   XLXI_17 : OR2
      port map (I0=>XLXN_17,
                I1=>XLXN_18,
                O=>XLXN_16);
   
   XLXI_18 : OR3
      port map (I0=>XLXN_23,
                I1=>XLXN_24,
                I2=>XLXN_25,
                O=>XLXN_20);
   
   XLXI_19 : OR3
      port map (I0=>XLXN_27,
                I1=>XLXN_28,
                I2=>XLXN_29,
                O=>XLXN_21);
   
   XLXI_22 : AND4
      port map (I0=>XLXN_6,
                I1=>z,
                I2=>XLXN_2,
                I3=>q1_DUMMY,
                O=>XLXN_23);
   
   XLXI_23 : AND4
      port map (I0=>XLXN_6,
                I1=>left,
                I2=>q0_DUMMY,
                I3=>XLXN_4,
                O=>XLXN_24);
   
   XLXI_24 : AND3
      port map (I0=>q2_DUMMY,
                I1=>left,
                I2=>XLXN_2,
                O=>XLXN_25);
   
   XLXI_25 : AND3
      port map (I0=>XLXN_2,
                I1=>XLXN_4,
                I2=>z,
                O=>XLXN_27);
   
   XLXI_26 : AND3
      port map (I0=>XLXN_2,
                I1=>XLXN_6,
                I2=>z,
                O=>XLXN_28);
   
   XLXI_27 : AND3
      port map (I0=>q0_DUMMY,
                I1=>XLXN_4,
                I2=>z,
                O=>XLXN_29);
   
   XLXI_28 : AND3
      port map (I0=>q0_DUMMY,
                I1=>XLXN_4,
                I2=>q2_DUMMY,
                O=>y);
   
   XLXI_36 : INV
      port map (I=>z,
                O=>left);
   
end BEHAVIORAL;


