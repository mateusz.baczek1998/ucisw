<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="q1" />
        <signal name="XLXN_4" />
        <signal name="q2" />
        <signal name="XLXN_6" />
        <signal name="z" />
        <signal name="left">
        </signal>
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="q0" />
        <signal name="y" />
        <signal name="zegar" />
        <signal name="XLXN_67" />
        <port polarity="Output" name="q1" />
        <port polarity="Output" name="q2" />
        <port polarity="Input" name="z" />
        <port polarity="Output" name="q0" />
        <port polarity="Output" name="y" />
        <port polarity="Input" name="zegar" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <block symbolname="inv" name="XLXI_4">
            <blockpin signalname="q0" name="I" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="q1" name="I" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="q2" name="I" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_2">
            <blockpin signalname="zegar" name="C" />
            <blockpin signalname="XLXN_20" name="D" />
            <blockpin signalname="q1" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <blockpin signalname="zegar" name="C" />
            <blockpin signalname="XLXN_21" name="D" />
            <blockpin signalname="q2" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_1">
            <blockpin signalname="zegar" name="C" />
            <blockpin signalname="XLXN_16" name="D" />
            <blockpin signalname="q0" name="Q" />
        </block>
        <block symbolname="or2" name="XLXI_17">
            <blockpin signalname="XLXN_17" name="I0" />
            <blockpin signalname="XLXN_18" name="I1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_18">
            <blockpin signalname="XLXN_23" name="I0" />
            <blockpin signalname="XLXN_24" name="I1" />
            <blockpin signalname="XLXN_25" name="I2" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_19">
            <blockpin signalname="XLXN_27" name="I0" />
            <blockpin signalname="XLXN_28" name="I1" />
            <blockpin signalname="XLXN_29" name="I2" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_16">
            <blockpin signalname="q0" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="XLXN_6" name="I2" />
            <blockpin signalname="z" name="I3" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_14">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="q1" name="I1" />
            <blockpin signalname="q2" name="I2" />
            <blockpin signalname="z" name="I3" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_22">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="z" name="I1" />
            <blockpin signalname="XLXN_2" name="I2" />
            <blockpin signalname="q1" name="I3" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_23">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="left" name="I1" />
            <blockpin signalname="q0" name="I2" />
            <blockpin signalname="XLXN_4" name="I3" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_24">
            <blockpin signalname="q2" name="I0" />
            <blockpin signalname="left" name="I1" />
            <blockpin signalname="XLXN_2" name="I2" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_25">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="z" name="I2" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_26">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="z" name="I2" />
            <blockpin signalname="XLXN_28" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_27">
            <blockpin signalname="q0" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="z" name="I2" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_28">
            <blockpin signalname="q0" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="q2" name="I2" />
            <blockpin signalname="y" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_36">
            <blockpin signalname="z" name="I" />
            <blockpin signalname="left" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="XLXN_2">
            <wire x2="528" y1="768" y2="768" x1="224" />
            <wire x2="672" y1="768" y2="768" x1="528" />
            <wire x2="1024" y1="768" y2="768" x1="672" />
            <wire x2="1024" y1="768" y2="1088" x1="1024" />
            <wire x2="1696" y1="768" y2="768" x1="1024" />
            <wire x2="1696" y1="768" y2="1104" x1="1696" />
            <wire x2="2160" y1="768" y2="768" x1="1696" />
            <wire x2="2160" y1="768" y2="1136" x1="2160" />
            <wire x2="2432" y1="768" y2="768" x1="2160" />
            <wire x2="3328" y1="768" y2="768" x1="2432" />
            <wire x2="2432" y1="768" y2="1136" x1="2432" />
            <wire x2="528" y1="768" y2="1344" x1="528" />
            <wire x2="672" y1="704" y2="768" x1="672" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="368" y1="816" y2="816" x1="224" />
            <wire x2="368" y1="816" y2="1344" x1="368" />
            <wire x2="1360" y1="816" y2="816" x1="368" />
            <wire x2="1952" y1="816" y2="816" x1="1360" />
            <wire x2="2224" y1="816" y2="816" x1="1952" />
            <wire x2="2224" y1="816" y2="1136" x1="2224" />
            <wire x2="2832" y1="816" y2="816" x1="2224" />
            <wire x2="2832" y1="816" y2="1136" x1="2832" />
            <wire x2="3296" y1="816" y2="816" x1="2832" />
            <wire x2="3328" y1="816" y2="816" x1="3296" />
            <wire x2="3296" y1="816" y2="944" x1="3296" />
            <wire x2="3392" y1="944" y2="944" x1="3296" />
            <wire x2="3392" y1="944" y2="1072" x1="3392" />
            <wire x2="1360" y1="816" y2="1104" x1="1360" />
            <wire x2="1952" y1="704" y2="816" x1="1952" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="432" y1="864" y2="864" x1="224" />
            <wire x2="432" y1="864" y2="1344" x1="432" />
            <wire x2="896" y1="864" y2="864" x1="432" />
            <wire x2="896" y1="864" y2="1088" x1="896" />
            <wire x2="1168" y1="864" y2="864" x1="896" />
            <wire x2="1168" y1="864" y2="1104" x1="1168" />
            <wire x2="2496" y1="864" y2="864" x1="1168" />
            <wire x2="3120" y1="864" y2="864" x1="2496" />
            <wire x2="3328" y1="864" y2="864" x1="3120" />
            <wire x2="2496" y1="864" y2="1136" x1="2496" />
            <wire x2="3120" y1="704" y2="864" x1="3120" />
        </branch>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="140" y="760">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="140" y="808">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="140" y="856">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="176" y="916">Z</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="364" y="760">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="364" y="808">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="364" y="856">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="400" y="916">Z</text>
        <instance x="640" y="480" name="XLXI_4" orien="R90" />
        <instance x="1920" y="480" name="XLXI_5" orien="R90" />
        <instance x="3088" y="480" name="XLXI_6" orien="R90" />
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="780" y="760">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="780" y="808">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="780" y="856">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="588" y="760">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="588" y="808">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="588" y="856">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1884" y="760">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1884" y="808">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1884" y="856">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3036" y="760">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3036" y="808">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3036" y="856">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2092" y="760">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2092" y="808">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2092" y="856">Q_2</text>
        <instance x="1616" y="2304" name="XLXI_2" orien="R0" />
        <instance x="2768" y="2288" name="XLXI_3" orien="R0" />
        <instance x="336" y="2304" name="XLXI_1" orien="R0" />
        <instance x="96" y="1744" name="XLXI_17" orien="R90" />
        <branch name="XLXN_16">
            <wire x2="192" y1="2000" y2="2048" x1="192" />
            <wire x2="336" y1="2048" y2="2048" x1="192" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="400" y1="1664" y2="1664" x1="160" />
            <wire x2="160" y1="1664" y2="1744" x1="160" />
            <wire x2="400" y1="1600" y2="1664" x1="400" />
        </branch>
        <instance x="1328" y="1648" name="XLXI_18" orien="R90" />
        <branch name="XLXN_20">
            <wire x2="1456" y1="1904" y2="2048" x1="1456" />
            <wire x2="1616" y1="2048" y2="2048" x1="1456" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="2576" y1="1904" y2="2032" x1="2576" />
            <wire x2="2768" y1="2032" y2="2032" x1="2576" />
        </branch>
        <instance x="2448" y="1648" name="XLXI_19" orien="R90" />
        <instance x="464" y="1344" name="XLXI_14" orien="R90" />
        <branch name="XLXN_18">
            <wire x2="224" y1="1728" y2="1744" x1="224" />
            <wire x2="624" y1="1728" y2="1728" x1="224" />
            <wire x2="624" y1="1600" y2="1728" x1="624" />
        </branch>
        <instance x="240" y="1344" name="XLXI_16" orien="R90" />
        <instance x="832" y="1088" name="XLXI_22" orien="R90" />
        <instance x="1104" y="1104" name="XLXI_23" orien="R90" />
        <branch name="XLXN_23">
            <wire x2="992" y1="1344" y2="1648" x1="992" />
            <wire x2="1392" y1="1648" y2="1648" x1="992" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="1264" y1="1360" y2="1504" x1="1264" />
            <wire x2="1456" y1="1504" y2="1504" x1="1264" />
            <wire x2="1456" y1="1504" y2="1648" x1="1456" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="1632" y1="1648" y2="1648" x1="1520" />
            <wire x2="1632" y1="1360" y2="1648" x1="1632" />
        </branch>
        <instance x="1504" y="1104" name="XLXI_24" orien="R90" />
        <instance x="2096" y="1136" name="XLXI_25" orien="R90" />
        <instance x="2368" y="1136" name="XLXI_26" orien="R90" />
        <instance x="2704" y="1136" name="XLXI_27" orien="R90" />
        <branch name="XLXN_27">
            <wire x2="2224" y1="1392" y2="1648" x1="2224" />
            <wire x2="2512" y1="1648" y2="1648" x1="2224" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="2496" y1="1392" y2="1520" x1="2496" />
            <wire x2="2576" y1="1520" y2="1520" x1="2496" />
            <wire x2="2576" y1="1520" y2="1648" x1="2576" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="2832" y1="1648" y2="1648" x1="2640" />
            <wire x2="2832" y1="1392" y2="1648" x1="2832" />
        </branch>
        <instance x="3264" y="1072" name="XLXI_28" orien="R90" />
        <branch name="y">
            <wire x2="3392" y1="1328" y2="1376" x1="3392" />
        </branch>
        <iomarker fontsize="28" x="3392" y="1376" name="y" orien="R90" />
        <iomarker fontsize="28" x="752" y="2416" name="zegar" orien="R0" />
        <branch name="zegar">
            <wire x2="336" y1="2176" y2="2176" x1="256" />
            <wire x2="256" y1="2176" y2="2304" x1="256" />
            <wire x2="336" y1="2304" y2="2304" x1="256" />
            <wire x2="336" y1="2304" y2="2336" x1="336" />
            <wire x2="448" y1="2336" y2="2336" x1="336" />
            <wire x2="1632" y1="2336" y2="2336" x1="448" />
            <wire x2="2784" y1="2336" y2="2336" x1="1632" />
            <wire x2="3264" y1="2336" y2="2336" x1="2784" />
            <wire x2="448" y1="2336" y2="2416" x1="448" />
            <wire x2="608" y1="2416" y2="2416" x1="448" />
            <wire x2="752" y1="2416" y2="2416" x1="608" />
            <wire x2="608" y1="2416" y2="2464" x1="608" />
            <wire x2="608" y1="2464" y2="2464" x1="576" />
            <wire x2="1616" y1="2176" y2="2176" x1="1552" />
            <wire x2="1552" y1="2176" y2="2304" x1="1552" />
            <wire x2="1632" y1="2304" y2="2304" x1="1552" />
            <wire x2="1632" y1="2304" y2="2336" x1="1632" />
            <wire x2="2768" y1="2160" y2="2160" x1="2704" />
            <wire x2="2704" y1="2160" y2="2288" x1="2704" />
            <wire x2="2784" y1="2288" y2="2288" x1="2704" />
            <wire x2="2784" y1="2288" y2="2336" x1="2784" />
            <wire x2="3264" y1="2320" y2="2336" x1="3264" />
        </branch>
        <branch name="z">
            <wire x2="720" y1="912" y2="912" x1="496" />
            <wire x2="720" y1="912" y2="1344" x1="720" />
            <wire x2="960" y1="912" y2="912" x1="720" />
            <wire x2="960" y1="912" y2="1088" x1="960" />
            <wire x2="2288" y1="912" y2="912" x1="960" />
            <wire x2="2288" y1="912" y2="1136" x1="2288" />
            <wire x2="2560" y1="912" y2="912" x1="2288" />
            <wire x2="2560" y1="912" y2="1136" x1="2560" />
            <wire x2="2896" y1="912" y2="912" x1="2560" />
            <wire x2="3328" y1="912" y2="912" x1="2896" />
            <wire x2="2896" y1="912" y2="1136" x1="2896" />
            <wire x2="496" y1="912" y2="928" x1="496" />
            <wire x2="496" y1="928" y2="1344" x1="496" />
            <wire x2="880" y1="928" y2="928" x1="496" />
            <wire x2="880" y1="928" y2="2528" x1="880" />
            <wire x2="880" y1="2528" y2="2640" x1="880" />
            <wire x2="880" y1="2528" y2="2528" x1="576" />
            <wire x2="880" y1="2640" y2="2640" x1="816" />
        </branch>
        <branch name="left">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="576" y="2592" type="branch" />
            <wire x2="640" y1="2592" y2="2592" x1="576" />
            <wire x2="576" y1="2592" y2="2640" x1="576" />
            <wire x2="592" y1="2640" y2="2640" x1="576" />
            <wire x2="1120" y1="2512" y2="2512" x1="640" />
            <wire x2="640" y1="2512" y2="2592" x1="640" />
            <wire x2="1232" y1="928" y2="928" x1="1120" />
            <wire x2="1232" y1="928" y2="1104" x1="1232" />
            <wire x2="1632" y1="928" y2="928" x1="1232" />
            <wire x2="3328" y1="928" y2="928" x1="1632" />
            <wire x2="1632" y1="928" y2="1104" x1="1632" />
            <wire x2="1120" y1="928" y2="2512" x1="1120" />
        </branch>
        <iomarker fontsize="28" x="576" y="2528" name="z" orien="R180" />
        <instance x="816" y="2672" name="XLXI_36" orien="M0" />
        <branch name="q2">
            <wire x2="656" y1="848" y2="848" x1="224" />
            <wire x2="656" y1="848" y2="1344" x1="656" />
            <wire x2="1568" y1="848" y2="848" x1="656" />
            <wire x2="1568" y1="848" y2="1104" x1="1568" />
            <wire x2="3184" y1="848" y2="848" x1="1568" />
            <wire x2="3312" y1="848" y2="848" x1="3184" />
            <wire x2="3328" y1="848" y2="848" x1="3312" />
            <wire x2="3312" y1="848" y2="896" x1="3312" />
            <wire x2="3456" y1="896" y2="896" x1="3312" />
            <wire x2="3456" y1="896" y2="1072" x1="3456" />
            <wire x2="3120" y1="416" y2="480" x1="3120" />
            <wire x2="3168" y1="416" y2="416" x1="3120" />
            <wire x2="3168" y1="416" y2="2032" x1="3168" />
            <wire x2="3184" y1="416" y2="416" x1="3168" />
            <wire x2="3184" y1="416" y2="848" x1="3184" />
            <wire x2="3168" y1="2032" y2="2032" x1="3152" />
            <wire x2="3184" y1="336" y2="416" x1="3184" />
        </branch>
        <branch name="q1">
            <wire x2="592" y1="800" y2="800" x1="224" />
            <wire x2="592" y1="800" y2="1344" x1="592" />
            <wire x2="1104" y1="800" y2="800" x1="592" />
            <wire x2="1104" y1="800" y2="1088" x1="1104" />
            <wire x2="2000" y1="800" y2="800" x1="1104" />
            <wire x2="3328" y1="800" y2="800" x1="2000" />
            <wire x2="1104" y1="1088" y2="1088" x1="1088" />
            <wire x2="1952" y1="432" y2="480" x1="1952" />
            <wire x2="2000" y1="432" y2="432" x1="1952" />
            <wire x2="2000" y1="432" y2="800" x1="2000" />
            <wire x2="2016" y1="432" y2="432" x1="2000" />
            <wire x2="2016" y1="432" y2="2048" x1="2016" />
            <wire x2="2016" y1="2048" y2="2048" x1="2000" />
            <wire x2="2016" y1="384" y2="400" x1="2016" />
            <wire x2="2016" y1="400" y2="432" x1="2016" />
        </branch>
        <branch name="q0">
            <wire x2="304" y1="752" y2="752" x1="224" />
            <wire x2="304" y1="752" y2="1344" x1="304" />
            <wire x2="736" y1="752" y2="752" x1="304" />
            <wire x2="1296" y1="752" y2="752" x1="736" />
            <wire x2="1296" y1="752" y2="1104" x1="1296" />
            <wire x2="2752" y1="752" y2="752" x1="1296" />
            <wire x2="2752" y1="752" y2="1136" x1="2752" />
            <wire x2="2768" y1="1136" y2="1136" x1="2752" />
            <wire x2="3264" y1="752" y2="752" x1="2752" />
            <wire x2="3328" y1="752" y2="752" x1="3264" />
            <wire x2="3264" y1="752" y2="1072" x1="3264" />
            <wire x2="3328" y1="1072" y2="1072" x1="3264" />
            <wire x2="672" y1="464" y2="480" x1="672" />
            <wire x2="736" y1="464" y2="464" x1="672" />
            <wire x2="736" y1="464" y2="608" x1="736" />
            <wire x2="736" y1="608" y2="752" x1="736" />
            <wire x2="752" y1="608" y2="608" x1="736" />
            <wire x2="752" y1="608" y2="2048" x1="752" />
            <wire x2="752" y1="2048" y2="2048" x1="720" />
            <wire x2="736" y1="400" y2="432" x1="736" />
            <wire x2="736" y1="432" y2="464" x1="736" />
        </branch>
        <iomarker fontsize="28" x="736" y="400" name="q0" orien="R270" />
        <iomarker fontsize="28" x="2016" y="384" name="q1" orien="R270" />
        <iomarker fontsize="28" x="3184" y="336" name="q2" orien="R270" />
    </sheet>
</drawing>