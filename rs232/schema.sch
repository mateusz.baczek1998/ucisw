<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="fd" name="Q0">
            <blockpin name="C" />
            <blockpin name="D" />
            <blockpin signalname="XLXN_2" name="Q" />
        </block>
        <block symbolname="fd" name="Q1">
            <blockpin name="C" />
            <blockpin name="D" />
            <blockpin signalname="XLXN_8" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_3">
            <blockpin signalname="XLXN_2" name="I" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_4">
            <blockpin signalname="XLXN_8" name="I" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="720" y="1216" name="Q0" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="0" y="-64" type="instance" />
        </instance>
        <instance x="1360" y="1216" name="Q1" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="0" y="-64" type="instance" />
        </instance>
        <branch name="XLXN_1">
            <wire x2="1200" y1="560" y2="560" x1="400" />
            <wire x2="2080" y1="560" y2="560" x1="1200" />
            <wire x2="1200" y1="560" y2="864" x1="1200" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1120" y1="528" y2="528" x1="400" />
            <wire x2="2080" y1="528" y2="528" x1="1120" />
            <wire x2="1120" y1="528" y2="960" x1="1120" />
            <wire x2="1120" y1="960" y2="1104" x1="1120" />
            <wire x2="1200" y1="1104" y2="1104" x1="1120" />
            <wire x2="1120" y1="960" y2="960" x1="1104" />
            <wire x2="1200" y1="1088" y2="1104" x1="1200" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="2080" y1="480" y2="480" x1="400" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="2080" y1="448" y2="448" x1="400" />
        </branch>
        <text style="fontsize:50;fontname:Arial;textcolor:rgb(255,0,255)" x="336" y="472">Z</text>
        <text style="fontsize:50;fontname:Arial;textcolor:rgb(255,0,255)" x="312" y="648">Q1</text>
        <text style="fontsize:50;fontname:Arial;textcolor:rgb(255,0,255)" x="312" y="552">Q0</text>
        <instance x="1168" y="1088" name="XLXI_3" orien="M270" />
        <instance x="1808" y="1088" name="XLXI_4" orien="M270" />
        <branch name="XLXN_7">
            <wire x2="1760" y1="656" y2="656" x1="400" />
            <wire x2="1840" y1="656" y2="656" x1="1760" />
            <wire x2="2080" y1="656" y2="656" x1="1840" />
            <wire x2="1840" y1="656" y2="864" x1="1840" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1760" y1="624" y2="624" x1="400" />
            <wire x2="1768" y1="624" y2="624" x1="1760" />
            <wire x2="1776" y1="624" y2="624" x1="1768" />
            <wire x2="2080" y1="624" y2="624" x1="1776" />
            <wire x2="1760" y1="624" y2="640" x1="1760" />
            <wire x2="1824" y1="640" y2="640" x1="1760" />
            <wire x2="1824" y1="640" y2="672" x1="1824" />
            <wire x2="1760" y1="960" y2="960" x1="1744" />
            <wire x2="1760" y1="960" y2="1104" x1="1760" />
            <wire x2="1840" y1="1104" y2="1104" x1="1760" />
            <wire x2="1824" y1="672" y2="672" x1="1760" />
            <wire x2="1760" y1="672" y2="704" x1="1760" />
            <wire x2="1760" y1="704" y2="960" x1="1760" />
            <wire x2="1840" y1="1088" y2="1104" x1="1840" />
        </branch>
    </sheet>
</drawing>