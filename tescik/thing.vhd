--TO JEST TO CO ROZKMINIALIMY CZYLI ZADANIE 2 
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:02:41 11/20/2019 
-- Design Name: 
-- Module Name:    module2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity module2 is
    Port ( X : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           RST : in  STD_LOGIC;
           Y : out  STD_LOGIC);
end module2;

architecture Behavioral of module2 is

signal Q : STD_LOGIC_VECTOR (2 downto 0) := "000";
signal D : STD_LOGIC_VECTOR (2 downto 0) := "000";
signal X_1 : STD_LOGIC_VECTOR (3 downto 0) := "0000";

begin

	X_1 <= Q & X;
	
	with Q select Y <= '1' when "100",
	'0' when others;
	
	D0 : FDC port map ( Q => Q(0), C => CLK, D => D(0), CLR => RST );
	D1 : FDC port map ( Q => Q(1), C => CLK, D => D(1), CLR => RST);
	D2 : FDC port map ( Q => Q(2), C => CLK, D => D(2), CLR => RST);
	
	with X_1 select D <= "000" when "0000",
								"001" when "0001",
								"010" when "0010",
								"001" when "0011",
								"000" when "0100",
								"011" when "0101",
								"010" when "0110",
								"100" when "0111",
								"010" when "1000",
								"101" when "1001",
								"000" when "1010",
								"001" when "1011",
								
								"000" when others;
end Behavioral;