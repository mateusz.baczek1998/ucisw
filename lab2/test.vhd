-- Vhdl test bench created from schematic C:\Users\lab\Downloads\ucisw-master\ucisw-master\lab2\licznik.sch - Tue Nov 12 19:43:29 2019
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY licznik_licznik_sch_tb IS
END licznik_licznik_sch_tb;
ARCHITECTURE behavioral OF licznik_licznik_sch_tb IS 

   COMPONENT licznik
   PORT( Q1	:	OUT	STD_LOGIC; 
          Q0	:	OUT	STD_LOGIC; 
          Q2	:	OUT	STD_LOGIC; 
          ZEGAR	:	IN	STD_LOGIC; 
          Q3	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL Q1	:	STD_LOGIC;
   SIGNAL Q0	:	STD_LOGIC;
   SIGNAL Q2	:	STD_LOGIC;
   SIGNAL ZEGAR	:	STD_LOGIC;
   SIGNAL Q3	:	STD_LOGIC;

BEGIN

   UUT: licznik PORT MAP(
		Q1 => Q1, 
		Q0 => Q0, 
		Q2 => Q2, 
		ZEGAR => ZEGAR, 
		Q3 => Q3
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		ZEGAR <= '0', '1' after 100ns, '0' after 200 ns, '1' after 300 ns, '0' after 400 ns, '1' after 500 ns, '0' after 600 ns, '1' after 700 ns, '0' after 800 ns, '1' after 900 ns, '0' after 1000 ns, '1' after 1100 ns, '0' after 1200 ns, '1' after 1300 ns, '0' after 1400 ns, '1' after 1500 ns;
	
	
	
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
