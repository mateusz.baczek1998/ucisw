<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_168" />
        <signal name="XLXN_169" />
        <signal name="XLXN_170" />
        <signal name="XLXN_171" />
        <signal name="XLXN_184" />
        <signal name="XLXN_186" />
        <signal name="XLXN_187" />
        <signal name="Q1" />
        <signal name="Q0" />
        <signal name="Q2" />
        <signal name="XLXN_195" />
        <signal name="XLXN_196" />
        <signal name="XLXN_198" />
        <signal name="XLXN_199" />
        <signal name="XLXN_201" />
        <signal name="XLXN_202" />
        <signal name="XLXN_203" />
        <signal name="XLXN_204" />
        <signal name="XLXN_205" />
        <signal name="XLXN_206" />
        <signal name="ZEGAR" />
        <signal name="Q3" />
        <signal name="XLXN_211" />
        <signal name="XLXN_212" />
        <signal name="XLXN_213" />
        <signal name="XLXN_214" />
        <signal name="XLXN_215" />
        <signal name="XLXN_216" />
        <signal name="XLXN_218" />
        <signal name="XLXN_219" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q2" />
        <port polarity="Input" name="ZEGAR" />
        <port polarity="Output" name="Q3" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="fd" name="XLXI_21">
            <attr value="1" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="ZEGAR" name="C" />
            <blockpin signalname="XLXN_201" name="D" />
            <blockpin signalname="Q0" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_180">
            <attr value="1" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="ZEGAR" name="C" />
            <blockpin signalname="XLXN_184" name="D" />
            <blockpin signalname="Q1" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_181">
            <attr value="0" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="ZEGAR" name="C" />
            <blockpin signalname="XLXN_186" name="D" />
            <blockpin signalname="Q2" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_182">
            <attr value="0" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="ZEGAR" name="C" />
            <blockpin signalname="XLXN_187" name="D" />
            <blockpin signalname="Q3" name="Q" />
        </block>
        <block symbolname="and4" name="XLXI_162">
            <blockpin signalname="XLXN_218" name="I0" />
            <blockpin signalname="Q2" name="I1" />
            <blockpin signalname="XLXN_213" name="I2" />
            <blockpin signalname="Q0" name="I3" />
            <blockpin signalname="XLXN_168" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_163">
            <blockpin signalname="Q3" name="I0" />
            <blockpin signalname="XLXN_216" name="I1" />
            <blockpin signalname="Q1" name="I2" />
            <blockpin signalname="XLXN_195" name="I3" />
            <blockpin signalname="XLXN_169" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_166">
            <blockpin signalname="XLXN_168" name="I0" />
            <blockpin signalname="XLXN_169" name="I1" />
            <blockpin signalname="XLXN_170" name="I2" />
            <blockpin signalname="XLXN_171" name="I3" />
            <blockpin signalname="XLXN_184" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_188">
            <blockpin signalname="Q0" name="I" />
            <blockpin signalname="XLXN_195" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_189">
            <blockpin signalname="Q1" name="I" />
            <blockpin signalname="XLXN_213" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_190">
            <blockpin signalname="Q2" name="I" />
            <blockpin signalname="XLXN_216" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_191">
            <blockpin signalname="Q3" name="I" />
            <blockpin signalname="XLXN_218" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_192">
            <blockpin signalname="XLXN_203" name="I0" />
            <blockpin signalname="XLXN_202" name="I1" />
            <blockpin signalname="XLXN_201" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_193">
            <blockpin signalname="Q3" name="I0" />
            <blockpin signalname="XLXN_216" name="I1" />
            <blockpin signalname="XLXN_195" name="I2" />
            <blockpin signalname="XLXN_202" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_194">
            <blockpin signalname="XLXN_218" name="I0" />
            <blockpin signalname="Q2" name="I1" />
            <blockpin signalname="XLXN_195" name="I2" />
            <blockpin signalname="XLXN_203" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_195">
            <blockpin signalname="XLXN_218" name="I0" />
            <blockpin signalname="Q2" name="I1" />
            <blockpin signalname="Q1" name="I2" />
            <blockpin signalname="XLXN_195" name="I3" />
            <blockpin signalname="XLXN_206" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_196">
            <blockpin signalname="XLXN_218" name="I0" />
            <blockpin signalname="XLXN_216" name="I1" />
            <blockpin signalname="Q1" name="I2" />
            <blockpin signalname="Q0" name="I3" />
            <blockpin signalname="XLXN_205" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_197">
            <blockpin signalname="XLXN_218" name="I0" />
            <blockpin signalname="Q2" name="I1" />
            <blockpin signalname="XLXN_213" name="I2" />
            <blockpin signalname="XLXN_204" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_198">
            <blockpin signalname="XLXN_204" name="I0" />
            <blockpin signalname="XLXN_205" name="I1" />
            <blockpin signalname="XLXN_206" name="I2" />
            <blockpin signalname="XLXN_186" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_201">
            <blockpin signalname="XLXN_211" name="I0" />
            <blockpin signalname="XLXN_212" name="I1" />
            <blockpin signalname="XLXN_187" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_202">
            <blockpin signalname="Q3" name="I0" />
            <blockpin signalname="XLXN_216" name="I1" />
            <blockpin signalname="XLXN_213" name="I2" />
            <blockpin signalname="XLXN_211" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_203">
            <blockpin signalname="XLXN_218" name="I0" />
            <blockpin signalname="Q2" name="I1" />
            <blockpin signalname="Q1" name="I2" />
            <blockpin signalname="Q0" name="I3" />
            <blockpin signalname="XLXN_212" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_205">
            <blockpin signalname="XLXN_218" name="I0" />
            <blockpin signalname="Q2" name="I1" />
            <blockpin signalname="Q1" name="I2" />
            <blockpin signalname="XLXN_195" name="I3" />
            <blockpin signalname="XLXN_171" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_206">
            <blockpin signalname="Q3" name="I0" />
            <blockpin signalname="XLXN_216" name="I1" />
            <blockpin signalname="XLXN_213" name="I2" />
            <blockpin signalname="Q0" name="I3" />
            <blockpin signalname="XLXN_170" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="560" y="2016" name="XLXI_21" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-408" type="instance" />
        </instance>
        <text style="fontsize:50;fontname:Arial" x="700" y="1828">D_0</text>
        <instance x="1600" y="2016" name="XLXI_180" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-408" type="instance" />
        </instance>
        <text style="fontsize:50;fontname:Arial" x="1740" y="1828">D_1</text>
        <instance x="2640" y="2016" name="XLXI_181" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-408" type="instance" />
        </instance>
        <text style="fontsize:50;fontname:Arial" x="2780" y="1828">D_2</text>
        <instance x="3600" y="2016" name="XLXI_182" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-408" type="instance" />
        </instance>
        <text style="fontsize:50;fontname:Arial" x="3740" y="1828">D_3</text>
        <instance x="1072" y="1008" name="XLXI_162" orien="R90" />
        <instance x="1312" y="1008" name="XLXI_163" orien="R90" />
        <instance x="1440" y="1344" name="XLXI_166" orien="R90" />
        <branch name="XLXN_168">
            <wire x2="1232" y1="1264" y2="1344" x1="1232" />
            <wire x2="1504" y1="1344" y2="1344" x1="1232" />
        </branch>
        <branch name="XLXN_169">
            <wire x2="1568" y1="1264" y2="1264" x1="1472" />
            <wire x2="1568" y1="1264" y2="1344" x1="1568" />
        </branch>
        <branch name="XLXN_170">
            <wire x2="1632" y1="1264" y2="1344" x1="1632" />
            <wire x2="1712" y1="1264" y2="1264" x1="1632" />
        </branch>
        <branch name="XLXN_171">
            <wire x2="1952" y1="1344" y2="1344" x1="1696" />
            <wire x2="1952" y1="1264" y2="1344" x1="1952" />
        </branch>
        <branch name="XLXN_184">
            <wire x2="1600" y1="1600" y2="1760" x1="1600" />
        </branch>
        <branch name="XLXN_186">
            <wire x2="2640" y1="1584" y2="1760" x1="2640" />
        </branch>
        <branch name="XLXN_187">
            <wire x2="3600" y1="1600" y2="1760" x1="3600" />
        </branch>
        <branch name="Q1">
            <wire x2="1504" y1="688" y2="688" x1="288" />
            <wire x2="1504" y1="688" y2="1008" x1="1504" />
            <wire x2="1984" y1="688" y2="688" x1="1504" />
            <wire x2="2080" y1="688" y2="688" x1="1984" />
            <wire x2="2080" y1="688" y2="1760" x1="2080" />
            <wire x2="2736" y1="688" y2="688" x1="2080" />
            <wire x2="2736" y1="688" y2="1056" x1="2736" />
            <wire x2="3056" y1="688" y2="688" x1="2736" />
            <wire x2="3056" y1="688" y2="1056" x1="3056" />
            <wire x2="3760" y1="688" y2="688" x1="3056" />
            <wire x2="3760" y1="688" y2="1056" x1="3760" />
            <wire x2="4480" y1="688" y2="688" x1="3760" />
            <wire x2="1984" y1="688" y2="1008" x1="1984" />
            <wire x2="2080" y1="1760" y2="1760" x1="1984" />
            <wire x2="2080" y1="560" y2="592" x1="2080" />
            <wire x2="2080" y1="592" y2="688" x1="2080" />
            <wire x2="2160" y1="592" y2="592" x1="2080" />
        </branch>
        <branch name="Q0">
            <wire x2="1040" y1="640" y2="640" x1="288" />
            <wire x2="1040" y1="640" y2="1760" x1="1040" />
            <wire x2="1328" y1="640" y2="640" x1="1040" />
            <wire x2="1328" y1="640" y2="1008" x1="1328" />
            <wire x2="1808" y1="640" y2="640" x1="1328" />
            <wire x2="2048" y1="640" y2="640" x1="1808" />
            <wire x2="2800" y1="640" y2="640" x1="2048" />
            <wire x2="2800" y1="640" y2="1056" x1="2800" />
            <wire x2="3824" y1="640" y2="640" x1="2800" />
            <wire x2="3824" y1="640" y2="1056" x1="3824" />
            <wire x2="4480" y1="640" y2="640" x1="3824" />
            <wire x2="2048" y1="640" y2="912" x1="2048" />
            <wire x2="1808" y1="640" y2="1008" x1="1808" />
            <wire x2="1040" y1="1760" y2="1760" x1="944" />
            <wire x2="1040" y1="560" y2="592" x1="1040" />
            <wire x2="1040" y1="592" y2="640" x1="1040" />
            <wire x2="1136" y1="592" y2="592" x1="1040" />
        </branch>
        <instance x="1072" y="560" name="XLXI_188" orien="R270" />
        <instance x="2112" y="560" name="XLXI_189" orien="R270" />
        <instance x="3232" y="560" name="XLXI_190" orien="R270" />
        <instance x="4192" y="560" name="XLXI_191" orien="R270" />
        <branch name="XLXN_195">
            <wire x2="512" y1="656" y2="656" x1="288" />
            <wire x2="512" y1="656" y2="1088" x1="512" />
            <wire x2="720" y1="656" y2="656" x1="512" />
            <wire x2="960" y1="656" y2="656" x1="720" />
            <wire x2="1568" y1="656" y2="656" x1="960" />
            <wire x2="1568" y1="656" y2="1008" x1="1568" />
            <wire x2="2032" y1="656" y2="656" x1="1568" />
            <wire x2="3120" y1="656" y2="656" x1="2032" />
            <wire x2="3120" y1="656" y2="1056" x1="3120" />
            <wire x2="3664" y1="656" y2="656" x1="3120" />
            <wire x2="3664" y1="656" y2="976" x1="3664" />
            <wire x2="4480" y1="656" y2="656" x1="3664" />
            <wire x2="2032" y1="656" y2="928" x1="2032" />
            <wire x2="2048" y1="928" y2="928" x1="2032" />
            <wire x2="2048" y1="928" y2="1008" x1="2048" />
            <wire x2="720" y1="656" y2="1088" x1="720" />
            <wire x2="1040" y1="320" y2="320" x1="960" />
            <wire x2="1040" y1="320" y2="336" x1="1040" />
            <wire x2="960" y1="320" y2="656" x1="960" />
        </branch>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="216" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="216" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="216" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="216" y="796">Q_3</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="520" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="520" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="520" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="520" y="796">Q_3</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1352" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1352" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1352" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1352" y="796">Q_3</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1864" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1864" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1864" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="1864" y="796">Q_3</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2248" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2248" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2248" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2248" y="796">Q_3</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2856" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2856" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2856" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="2856" y="796">Q_3</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3384" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3384" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3384" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3384" y="796">Q_3</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3880" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3880" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3880" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="3880" y="796">Q_3</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="4520" y="652">Q_0</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="4520" y="700">Q_1</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="4520" y="748">Q_2</text>
        <text style="fontsize:30;fontname:Arial;textcolor:rgb(255,0,255)" x="4520" y="796">Q_3</text>
        <instance x="464" y="1344" name="XLXI_192" orien="R90" />
        <instance x="528" y="1088" name="XLXI_193" orien="R90" />
        <instance x="320" y="1088" name="XLXI_194" orien="R90" />
        <branch name="XLXN_201">
            <wire x2="560" y1="1600" y2="1760" x1="560" />
        </branch>
        <branch name="XLXN_202">
            <wire x2="656" y1="1344" y2="1344" x1="592" />
        </branch>
        <branch name="XLXN_203">
            <wire x2="528" y1="1344" y2="1344" x1="448" />
        </branch>
        <instance x="2544" y="1056" name="XLXI_196" orien="R90" />
        <instance x="2512" y="1328" name="XLXI_198" orien="R90" />
        <branch name="XLXN_204">
            <wire x2="2368" y1="1312" y2="1328" x1="2368" />
            <wire x2="2576" y1="1328" y2="1328" x1="2368" />
        </branch>
        <branch name="XLXN_205">
            <wire x2="2640" y1="1312" y2="1328" x1="2640" />
            <wire x2="2704" y1="1312" y2="1312" x1="2640" />
        </branch>
        <branch name="XLXN_206">
            <wire x2="3024" y1="1328" y2="1328" x1="2704" />
            <wire x2="3024" y1="1312" y2="1328" x1="3024" />
        </branch>
        <instance x="2240" y="1056" name="XLXI_197" orien="R90" />
        <instance x="2864" y="1056" name="XLXI_195" orien="R90" />
        <branch name="ZEGAR">
            <wire x2="560" y1="2000" y2="2000" x1="480" />
            <wire x2="1600" y1="2000" y2="2000" x1="560" />
            <wire x2="2640" y1="2000" y2="2000" x1="1600" />
            <wire x2="3600" y1="2000" y2="2000" x1="2640" />
            <wire x2="3664" y1="2000" y2="2000" x1="3600" />
            <wire x2="560" y1="1888" y2="2000" x1="560" />
            <wire x2="1600" y1="1888" y2="2000" x1="1600" />
            <wire x2="2640" y1="1888" y2="2000" x1="2640" />
            <wire x2="3600" y1="1888" y2="2000" x1="3600" />
        </branch>
        <iomarker fontsize="28" x="480" y="2000" name="ZEGAR" orien="R180" />
        <iomarker fontsize="28" x="1136" y="592" name="Q0" orien="R0" />
        <iomarker fontsize="28" x="2160" y="592" name="Q1" orien="R0" />
        <iomarker fontsize="28" x="3280" y="592" name="Q2" orien="R0" />
        <iomarker fontsize="28" x="4240" y="592" name="Q3" orien="R0" />
        <branch name="Q3">
            <wire x2="592" y1="784" y2="784" x1="288" />
            <wire x2="592" y1="784" y2="1088" x1="592" />
            <wire x2="1376" y1="784" y2="784" x1="592" />
            <wire x2="1376" y1="784" y2="1008" x1="1376" />
            <wire x2="1600" y1="784" y2="784" x1="1376" />
            <wire x2="1608" y1="784" y2="784" x1="1600" />
            <wire x2="1856" y1="784" y2="784" x1="1608" />
            <wire x2="3536" y1="784" y2="784" x1="1856" />
            <wire x2="4160" y1="784" y2="784" x1="3536" />
            <wire x2="4160" y1="784" y2="1760" x1="4160" />
            <wire x2="4208" y1="1760" y2="1760" x1="4160" />
            <wire x2="4208" y1="1760" y2="1840" x1="4208" />
            <wire x2="4160" y1="1760" y2="1840" x1="4160" />
            <wire x2="4208" y1="1840" y2="1840" x1="4160" />
            <wire x2="4480" y1="784" y2="784" x1="4160" />
            <wire x2="3536" y1="784" y2="1008" x1="3536" />
            <wire x2="1856" y1="784" y2="912" x1="1856" />
            <wire x2="1600" y1="784" y2="864" x1="1600" />
            <wire x2="1616" y1="864" y2="864" x1="1600" />
            <wire x2="1616" y1="864" y2="1008" x1="1616" />
            <wire x2="3536" y1="1008" y2="1008" x1="3424" />
            <wire x2="3424" y1="1008" y2="1056" x1="3424" />
            <wire x2="4160" y1="1760" y2="1760" x1="3984" />
            <wire x2="4160" y1="560" y2="592" x1="4160" />
            <wire x2="4160" y1="592" y2="784" x1="4160" />
            <wire x2="4240" y1="592" y2="592" x1="4160" />
        </branch>
        <instance x="3504" y="1344" name="XLXI_201" orien="R90" />
        <instance x="3360" y="1056" name="XLXI_202" orien="R90" />
        <branch name="XLXN_211">
            <wire x2="3488" y1="1312" y2="1328" x1="3488" />
            <wire x2="3568" y1="1328" y2="1328" x1="3488" />
            <wire x2="3568" y1="1328" y2="1344" x1="3568" />
        </branch>
        <instance x="3568" y="1056" name="XLXI_203" orien="R90" />
        <branch name="XLXN_212">
            <wire x2="3632" y1="1328" y2="1344" x1="3632" />
            <wire x2="3712" y1="1328" y2="1328" x1="3632" />
            <wire x2="3712" y1="1328" y2="1392" x1="3712" />
            <wire x2="3728" y1="1392" y2="1392" x1="3712" />
            <wire x2="3728" y1="1312" y2="1392" x1="3728" />
        </branch>
        <branch name="XLXN_213">
            <wire x2="1264" y1="704" y2="704" x1="288" />
            <wire x2="1264" y1="704" y2="1008" x1="1264" />
            <wire x2="1744" y1="704" y2="704" x1="1264" />
            <wire x2="2000" y1="704" y2="704" x1="1744" />
            <wire x2="2432" y1="704" y2="704" x1="2000" />
            <wire x2="2432" y1="704" y2="1056" x1="2432" />
            <wire x2="3632" y1="704" y2="704" x1="2432" />
            <wire x2="3632" y1="704" y2="880" x1="3632" />
            <wire x2="4480" y1="704" y2="704" x1="3632" />
            <wire x2="1744" y1="704" y2="1008" x1="1744" />
            <wire x2="2080" y1="320" y2="320" x1="2000" />
            <wire x2="2080" y1="320" y2="336" x1="2080" />
            <wire x2="2000" y1="320" y2="704" x1="2000" />
            <wire x2="3552" y1="880" y2="1056" x1="3552" />
            <wire x2="3632" y1="880" y2="880" x1="3552" />
        </branch>
        <instance x="1792" y="1008" name="XLXI_205" orien="R90" />
        <branch name="XLXN_216">
            <wire x2="656" y1="752" y2="752" x1="288" />
            <wire x2="656" y1="752" y2="1088" x1="656" />
            <wire x2="1440" y1="752" y2="752" x1="656" />
            <wire x2="1440" y1="752" y2="1008" x1="1440" />
            <wire x2="1680" y1="752" y2="752" x1="1440" />
            <wire x2="1696" y1="752" y2="752" x1="1680" />
            <wire x2="1920" y1="752" y2="752" x1="1696" />
            <wire x2="2672" y1="752" y2="752" x1="1920" />
            <wire x2="3040" y1="752" y2="752" x1="2672" />
            <wire x2="3600" y1="752" y2="752" x1="3040" />
            <wire x2="3600" y1="752" y2="1024" x1="3600" />
            <wire x2="4480" y1="752" y2="752" x1="3600" />
            <wire x2="2672" y1="752" y2="1056" x1="2672" />
            <wire x2="1920" y1="752" y2="912" x1="1920" />
            <wire x2="1680" y1="752" y2="1008" x1="1680" />
            <wire x2="3200" y1="320" y2="320" x1="3040" />
            <wire x2="3200" y1="320" y2="336" x1="3200" />
            <wire x2="3040" y1="320" y2="752" x1="3040" />
            <wire x2="3600" y1="1024" y2="1024" x1="3488" />
            <wire x2="3488" y1="1024" y2="1056" x1="3488" />
        </branch>
        <branch name="Q2">
            <wire x2="448" y1="736" y2="736" x1="288" />
            <wire x2="448" y1="736" y2="1088" x1="448" />
            <wire x2="1200" y1="736" y2="736" x1="448" />
            <wire x2="1200" y1="736" y2="1008" x1="1200" />
            <wire x2="1680" y1="736" y2="736" x1="1200" />
            <wire x2="1936" y1="736" y2="736" x1="1680" />
            <wire x2="2368" y1="736" y2="736" x1="1936" />
            <wire x2="2368" y1="736" y2="1056" x1="2368" />
            <wire x2="2992" y1="736" y2="736" x1="2368" />
            <wire x2="3200" y1="736" y2="736" x1="2992" />
            <wire x2="3200" y1="736" y2="1760" x1="3200" />
            <wire x2="3696" y1="736" y2="736" x1="3200" />
            <wire x2="3696" y1="736" y2="1056" x1="3696" />
            <wire x2="4480" y1="736" y2="736" x1="3696" />
            <wire x2="2992" y1="736" y2="1056" x1="2992" />
            <wire x2="1936" y1="736" y2="928" x1="1936" />
            <wire x2="1920" y1="928" y2="1008" x1="1920" />
            <wire x2="1936" y1="928" y2="928" x1="1920" />
            <wire x2="3200" y1="1760" y2="1760" x1="3024" />
            <wire x2="3200" y1="560" y2="592" x1="3200" />
            <wire x2="3200" y1="592" y2="736" x1="3200" />
            <wire x2="3280" y1="592" y2="592" x1="3200" />
        </branch>
        <branch name="XLXN_218">
            <wire x2="384" y1="800" y2="800" x1="288" />
            <wire x2="384" y1="800" y2="1088" x1="384" />
            <wire x2="1136" y1="800" y2="800" x1="384" />
            <wire x2="1136" y1="800" y2="1008" x1="1136" />
            <wire x2="1616" y1="800" y2="800" x1="1136" />
            <wire x2="1840" y1="800" y2="800" x1="1616" />
            <wire x2="2304" y1="800" y2="800" x1="1840" />
            <wire x2="2304" y1="800" y2="1056" x1="2304" />
            <wire x2="2608" y1="800" y2="800" x1="2304" />
            <wire x2="2608" y1="800" y2="1056" x1="2608" />
            <wire x2="2928" y1="800" y2="800" x1="2608" />
            <wire x2="2928" y1="800" y2="1056" x1="2928" />
            <wire x2="3616" y1="800" y2="800" x1="2928" />
            <wire x2="4080" y1="800" y2="800" x1="3616" />
            <wire x2="4480" y1="800" y2="800" x1="4080" />
            <wire x2="3616" y1="800" y2="1056" x1="3616" />
            <wire x2="3632" y1="1056" y2="1056" x1="3616" />
            <wire x2="1840" y1="800" y2="1008" x1="1840" />
            <wire x2="1856" y1="1008" y2="1008" x1="1840" />
            <wire x2="4160" y1="320" y2="320" x1="4080" />
            <wire x2="4160" y1="320" y2="336" x1="4160" />
            <wire x2="4080" y1="320" y2="800" x1="4080" />
        </branch>
        <instance x="1552" y="1008" name="XLXI_206" orien="R90" />
    </sheet>
</drawing>